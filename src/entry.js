import React from 'react'
import ReactDOM from 'react-dom'
import App from './App.js'

class Entry {
  async init(container, api, options) {
    this.api = api
    const renderPromise = new Promise((resolve) =>
      ReactDOM.render(
        <App getImagePath={(path) => (path.includes('base64') ? path : api.enginePath(path))}/>,
        container,
        resolve,
      ),
    )

    const cssPromise = api.loadCss(api.enginePath('static/css/main.css'))

    return Promise.all([renderPromise, cssPromise])
  }

  component() {}

  destroy(container) {
    ReactDOM.unmountComponentAtNode(container)
  }

  isStateValid() {}

  setState(stateData) {}

  getState() {}

  setStateFrozen(isFrozen) {}
}

export default Entry
