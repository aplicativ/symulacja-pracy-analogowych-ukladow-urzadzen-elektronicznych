const initialState = {
  level: 'easy',

  easy: {
    product: null,
    type: null,
    fabric: null,
    bannedFeatures: [],
    isValid: false,
    R1: null,
    R2:null,
    R3: null,
    R4: null,
  },
  average: {
    product: null,
    type: null,
    fabric: null,
    bannedFeatures: ['type'],
    isValid: false,
    IA: null,
    UR: null,
    UL: null,
    UC: null,
    FR: null,
    I: null,
  },
  hard: {
    step: 1,
    product: null,
    type: null,
    fabric: null,
    bannedFeatures: [],
    isValid: false,
  },
  veryHard: {
    product: null,
    type: null,
    fabric: null,
    bannedFeatures: [],
    isValid: false,
  },
};

export default initialState;
