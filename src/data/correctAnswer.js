const easyDataCorrect = {
  R1: "100k",
  R2: "50k",
  R3: "150k",
  R4: "50k",
};

const averageDataCorrect = {
  IA: "7,07",
  UR: "7,07",
  UL: "0,044",
  UC: "0,11",
  FR: "5033",
  I: "7,07",
};

const allAnswerData = {
  easyDataCorrect,
  averageDataCorrect,
};

export {  allAnswerData };