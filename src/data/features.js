import dioda from './img/Dioda.png';
import kompaktor from './img/KompaktorLM311.png';
import kondensator100 from './img/Kondensator_100nF.png';
import kondensator200 from './img/Kondensator_200.png';
import potencjometr from './img/Potencjometr_5kOhm.png';
import R1m from './img/R_1MOhm.png';
import R56 from './img/R_56kOhm.png';
import R330 from './img/R_330Ohm.png';
import termistor from './img/Termistor.png';

import diodaImg from './img/dioda_img.png';
import kompaktorImg from './img/kompaktor_img.png';
import kondensator100Img from './img/kondensator100img.png';
import kondensator200Img from './img/kondensator200img.png';
import potencjometrImg from './img/potencjometr_img.png';
import R1kImg from './img/rezystor1_img.png';
import R2kImg from './img/rezystor1_img2.png';
import R1mImg from './img/rezystor1m_img.png';
import R56Img from './img/rezystor56_img.png';
import R330Img from './img/rezystor330.png';
import termistorImg from './img/termistor_img.png';

import przewody1 from './img/przewody1.png';
import przewody2 from './img/przewody2.png';
import przewody3 from './img/przewody3.png';
import przewody4 from './img/przewody4.png';
import przewody5 from './img/przewody5.png';
import przewody6 from './img/przewody6.png';

const productData = [
  {
    key: '100k',
    name: '100k',
    level: 'easy',
  },
  {
    key: '7,07',
    name: '7,07',
    level: 'average',
  },
  {
    key: '10',
    name: '10',
    level: 'average',
  },
  {
    key: '19',
    name: '19',
    level: 'average',
  },
  {
    key: '25',
    name: '25',
    level: 'average',
  },
  {
    key: '150k',
    name: '150k',
    level: 'easy',
  },
  {
    key: '200k',
    name: '200k',
    level: 'easy',
  },
  {
    key: '250k',
    name: '250k',
    level: 'easy',
  },
  {
    key: 'chaqueta',
    name: 'Żakiet',
    level: 'hard',
  },
];

const typesData = [
  { key: '20k', name: '20k', level: 'easy' },
  { key: '30k', name: '30k', level: 'easy' },
  { key: '40k', name: '40k', level: 'easy' },
  { key: '50k', name: '50k', level: 'easy' },
  { key: '5,3', name: '5,3', level: 'average' },
  { key: '7,4', name: '7,4', level: 'average' },
  { key: '8,22', name: '8,22', level: 'average' },
  { key: '9,89', name: '9,89', level: 'average' },
];

const fabricsData = [
  {
    items: [
      { key: '20k', name: '20k', level: 'easy' },
      { key: '50k', name: '50k', level: 'easy' },
      { key: '100k', name: '100k', level: 'easy' },
      { key: '150k', name: '150k', level: 'easy' },
      { key: '0,06', name: '0,06', level: 'average' },
      { key: '2,5', name: '2,5', level: 'average' },
      { key: '4,44', name: '4,44', level: 'average' },
      { key: '9,85', name: '9,85', level: 'average' },
    ],
  },
];

const R4Data = [
  {
    items: [
      { key: '20k', name: '20k', level: 'easy' },
      { key: '30k', name: '30k', level: 'easy' },
      { key: '40k', name: '40k', level: 'easy' },
      { key: '50k', name: '50k', level: 'easy' },
      { key: '1,25', name: '1,25', level: 'average' },
      { key: '2,2', name: '2,2', level: 'average' },
      { key: '7,45', name: '7,45', level: 'average' },
      { key: '10,01', name: '10,01', level: 'average' },
    ],
  },
];

const IAData = [
  {
    items: [
      { key: '7,07', name: '7,07', level: 'average' },
      { key: '10', name: '10', level: 'average' },
      { key: '19', name: '19', level: 'average' },
      { key: '25', name: '25', level: 'average' },
    ],
  },
];

const URData = [
  {
    items: [
      { key: '5,3', name: '5,3', level: 'average' },
      { key: '7,4', name: '7,4', level: 'average' },
      { key: '8,22', name: '8,22', level: 'average' },
      { key: '7,07', name: '7,07', level: 'average' },
    ],
  },
];

const ULData = [
  {
    items: [
      { key: '0,044', name: '0,044', level: 'average' },
      { key: '2,5', name: '2,5', level: 'average' },
      { key: '4,44', name: '4,44', level: 'average' },
      { key: '9,85', name: '9,85', level: 'average' },
    ],
  },
];
const UCData = [
  {
    items: [
      { key: '1,25', name: '1,25', level: 'average' },
      { key: '2,2', name: '2,2', level: 'average' },
      { key: '7,45', name: '7,45', level: 'average' },
      { key: '0,11', name: '0,11', level: 'average' },
    ],
  },
];

const FRData = [
  {
    items: [
      { key: '5033', name: '5033', level: 'average' },
      { key: '6033', name: '6033', level: 'average' },
      { key: '7033', name: '7033', level: 'average' },
      { key: '8033', name: '8033', level: 'average' },
    ],
  },
];

const IData = [
  {
    items: [
      { key: '1', name: '1', level: 'average' },
      { key: '10', name: '10', level: 'average' },
      { key: '100', name: '100', level: 'average' },
      { key: '7,07', name: '7,07', level: 'average' },
    ],
  },
];

const taskVeryHardMenu = {
  menu: {
    id: 'menu',
    list: [
      { key: '1', text: 'większa' },
      {
        key: '2',
        text: 'mniejsza',
      },
    ],
  },

  content1: {
    id: 'content1',
    list: [],
  },
  content2: {
    id: 'content2',
    list: [],
  },
};

const taskVeryHardCorrectAnswer = {
  content1: 'mniejsza',
  content2: 'większa',
};

const taskVeryHardPDF = ['1.', '2.'];

const taskHardMenu = {
  menu: {
    id: 'menu',
    list: [
      {
        key: '1',
        text: 'komparator typu LM311',
        name: 'komparator typu LM311',
        icon: kompaktor,
        img: kompaktorImg,
        step: 1,
      },
      {
        key: '2',
        text: 'termistor',
        name: 'termistor',
        icon: termistor,
        img: termistorImg,
        step: 1,
      },
      {
        key: '3',
        text: 'rezystor o wartości 1 kΩ (1)',
        name: 'rezystor o wartości 1 kΩ',
        icon: R1kImg,
        img: R1kImg,
        step: 1,
      },
      {
        key: '4',
        text: 'kondensator o pojemności 100 nF',
        name: 'kondensator o pojemności 100 nF',
        icon: kondensator100,
        img: kondensator100Img,
        step: 1,
      },
      {
        key: '5',
        text: 'kondensator o pojemności 220 μF',
        name: 'kondensator o pojemności 220 μF',
        icon: kondensator200,
        img: kondensator200Img,
        step: 1,
      },
      {
        key: '6',
        text: 'rezystor o wartości 330 Ω',
        name: 'rezystor o wartości 330 Ω',
        icon: R330,
        img: R330Img,
        step: 1,
      },
      {
        key: '7',
        text: 'rezystor o wartości 1 kΩ (2)',
        name: 'rezystor o wartości 1 kΩ',
        icon: R2kImg,
        img: R2kImg,
        step: 1,
      },
      {
        key: '8',
        text: 'rezystor o wartości 56 kΩ',
        name: 'rezystor o wartości 56 kΩ',
        icon: R56,
        img: R56Img,
        step: 1,
      },
      {
        key: '9',
        text: 'rezystor o wartości 1 MΩ',
        name: 'rezystor o wartości 1 MΩ',
        icon: R1m,
        img: R1mImg,
        step: 1,
      },
      {
        key: '10',
        text: 'potencjometr o wartości maksymalnej opornosci 5 kΩ',
        name: 'potencjometr o wartości maksymalnej opornosci 5 kΩ',
        icon: potencjometr,
        img: potencjometrImg,
        step: 1,
      },
      {
        key: '11',
        text: 'dioda świecąca',
        name: 'dioda świecąca',
        icon: dioda,
        img: diodaImg,
        step: 1,
      },
      {
        key: '12',
        text: 'przewody połączeniowe (1)',
        name: 'przewody połączeniowe',
        icon: przewody1,
        img: przewody1,
        step: 2,
      },
      {
        key: '13',
        text: 'przewody połączeniowe (2)',
        name: 'przewody połączeniowe',
        icon: przewody2,
        img: przewody2,
        step: 2,
      },
      {
        key: '14',
        text: 'przewody połączeniowe (3)',
        name: 'przewody połączeniowe',
        icon: przewody3,
        img: przewody3,
        step: 2,
      },
      {
        key: '15',
        text: 'przewody połączeniowe (4)',
        name: 'przewody połączeniowe',
        icon: przewody4,
        img: przewody4,
        step: 2,
      },
      {
        key: '16',
        text: 'przewody połączeniowe (5)',
        name: 'przewody połączeniowe',
        icon: przewody5,
        img: przewody5,
        step: 2,
      },
      {
        key: '17',
        text: 'przewody połączeniowe (6)',
        name: 'przewody połączeniowe',
        icon: przewody6,
        img: przewody6,
        step: 2,
      },
    ],
  },

  content1: {
    id: 'content1',
    list: [],
  },
  content2: {
    id: 'content2',
    list: [],
  },
  content3: {
    id: 'content3',
    list: [],
  },
  content4: {
    id: 'content4',
    list: [],
  },
  content5: {
    id: 'content5',
    list: [],
  },
  content6: {
    id: 'content6',
    list: [],
  },
  content7: {
    id: 'content7',
    list: [],
  },
  content8: {
    id: 'content8',
    list: [],
  },
  content9: {
    id: 'content9',
    list: [],
  },
  content10: {
    id: 'content10',
    list: [],
  },
  content11: {
    id: 'content11',
    list: [],
  },
  content12: {
    id: 'content12',
    list: [],
  },
  content13: {
    id: 'content13',
    list: [],
  },
  content14: {
    id: 'content14',
    list: [],
  },
  content15: {
    id: 'content15',
    list: [],
  },
  content16: {
    id: 'content16',
    list: [],
  },
  content17: {
    id: 'content17',
    list: [],
  },
};

const taskHardPDF = [
  'dioda świecąca',
  'komparator typu LM311',
  'kondensator o pojemności 100 nF',
  'kondensator o pojemności 220 μF',
  'potencjometr o wartości maksymalnej opornosci 5 kΩ',
  'rezystor o wartości 1 kΩ (1)',
  'rezystor o wartości 1 kΩ (2)',
  'rezystor o wartości 1 MΩ',
  'rezystor o wartości 56 kΩ',
  'rezystor o wartości 330 Ω',
  'termistor',
  'przewody połączeniowe (1)',
  'przewody połączeniowe (2)',
  'przewody połączeniowe (3)',
  'przewody połączeniowe (4)',
  'przewody połączeniowe (5)',
];

const taskHardCorrectAnswer = {
  content1: 'dioda świecąca',
  content2: 'komparator typu LM311',
  content3: 'kondensator o pojemności 100 nF',
  content4: 'kondensator o pojemności 220 μF',
  content5: 'potencjometr o wartości maksymalnej opornosci 5 kΩ',
  content6: 'rezystor o wartości 1 kΩ (1)',
  content7: 'rezystor o wartości 1 kΩ (2)',
  content8: 'rezystor o wartości 1 MΩ',
  content9: 'rezystor o wartości 56 kΩ',
  content10: 'rezystor o wartości 330 Ω',
  content11: 'termistor',
  content12: 'przewody połączeniowe (1)',
  content13: 'przewody połączeniowe (2)',
  content14: 'przewody połączeniowe (3)',
  content15: 'przewody połączeniowe (4)',
  content16: 'przewody połączeniowe (5)',
  content17: 'przewody połączeniowe (6)',
};

const stepperLabel = ['Etap I', 'Etap II'];

export {
  typesData,
  fabricsData,
  R4Data,
  IAData,
  URData,
  ULData,
  UCData,
  FRData,
  IData,
  taskVeryHardMenu,
  taskVeryHardCorrectAnswer,
  taskHardMenu,
  productData,
  stepperLabel,
  taskHardCorrectAnswer,
  taskHardPDF,
  taskVeryHardPDF,
};
