import React from 'react';
import './style.css';

const sineWave = () => {};

export default function Resistor({ R1, R2, R3, R4 }) {
  return (
    <div style={{ width: '100%' }}>
      <svg
        id="Warstwa_1"
        data-name="Warstwa 1"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 850 450"
      >
        <defs>
          <style
            dangerouslySetInnerHTML={{
              __html:
                '.cls-1,.cls-2,.cls-3{fill:none;stroke-miterlimit:10;}.cls-1{stroke:#2a4b9b;}.cls-2{stroke:#2b4c9a;}.cls-3{stroke:#3f9e3d;}.cls-4,.cls-5{fill:#2b4c9a;}.cls-5,.cls-6,.cls-8{font-size:13.02px;}.cls-10,.cls-5,.cls-6,.cls-8{font-family:MyriadPro-Regular, Myriad Pro;}.cls-10,.cls-6,.cls-8{fill:#3f9e3d;}.cls-7{letter-spacing:-0.01em;}.cls-8{letter-spacing:-0.01em;}.cls-9{letter-spacing:0em;}.cls-10{font-size:7.1px;}.cls-11{font-size:26.05px;fill:red;font-family:Lato-Black, Lato;font-weight:800;}',
            }}
          />
        </defs>
        <line className="cls-1" x1="85.09" y1="254.91" x2="85.26" y2="281.62" />
        <line className="cls-1" x1="85.08" y1="215.31" x2="85.08" y2="227.4" />
        <polygon
          className="cls-2"
          points="92.1 281.62 85.28 281.62 78.46 281.62 81.87 285.17 85.28 288.72 88.69 285.17 92.1 281.62"
        />
        <polygon
          className="cls-2"
          points="225.47 333.29 218.65 333.29 211.82 333.29 215.23 336.84 218.65 340.39 222.06 336.84 225.47 333.29"
        />
        <line className="cls-3" x1="85.2" y1="234.91" x2="85.2" y2="229.76" />
        <line className="cls-3" x1="82.59" y1="232.4" x2="87.77" y2="232.4" />
        <line className="cls-3" x1="82.23" y1="250.76" x2="88.18" y2="250.76" />
        <path
          className="cls-2"
          d="M85.08,227.4a13.76,13.76,0,1,1-13.75,13.76A13.76,13.76,0,0,1,85.08,227.4"
        />
        <path
          className="cls-2"
          d="M77.77,242.27s0-4.46,3.72-4.46,3,6.69,7.43,6.69c3,0,3.72-3.71,3.72-3.71"
        />
        <line
          className="cls-3"
          x1="84.43"
          y1="215.06"
          x2="157.13"
          y2="215.06"
        />
        <line
          className="cls-2"
          x1="190.62"
          y1="214.99"
          x2="205.56"
          y2="214.99"
        />
        <line
          className="cls-2"
          x1="300.7"
          y1="309.08"
          x2="326.48"
          y2="309.08"
        />
        <line
          className="cls-2"
          x1="300.07"
          y1="300.54"
          x2="325.85"
          y2="300.54"
        />
        <line
          className="cls-2"
          x1="157.13"
          y1="215.06"
          x2="166.36"
          y2="215.06"
        />
        <line
          className="cls-2"
          x1="306.69"
          y1="313.94"
          x2="319.63"
          y2="313.94"
        />
        <line
          className="cls-2"
          x1="306.7"
          y1="304.22"
          x2="319.64"
          y2="304.22"
        />
        <path
          className="cls-3"
          d="M166.36,215.64c.66-1.15,2.59-4.35,3.25-5.5,1.32,3.07,2.86,6.54,4.18,9.61,1.3-3.12,2.55-6.3,3.82-9.37,2,4.6,2.49,5.65,3.82,9.09,1.1-3.25,2.3-5.64,3.4-8.89,1.32,3.25,2,6,3.37,9.26.81-1.67,1.31-2.7,2.61-5.35"
        />
        <line
          className="cls-2"
          x1="486.95"
          y1="241.38"
          x2="499.7"
          y2="241.38"
        />
        <line
          className="cls-2"
          x1="448.94"
          y1="241.38"
          x2="461.33"
          y2="241.38"
        />
        <path
          className="cls-3"
          d="M461,242c.7-1.15,2.77-4.35,3.48-5.49,1.41,3.06,3.05,6.53,4.46,9.6,1.39-3.12,2.73-6.3,4.08-9.37,2.12,4.6,2.67,5.65,4.09,9.09,1.17-3.25,2.46-5.64,3.63-8.89,1.42,3.25,2.19,6,3.61,9.26.86-1.67,1.39-2.7,2.79-5.35"
        />
        <line className="cls-3" x1="204.95" y1="80.33" x2="264.23" y2="80.33" />
        <line className="cls-3" x1="312.41" y1="80.26" x2="446.35" y2="80.26" />
        <line className="cls-2" x1="298.76" y1="80.33" x2="315.5" y2="80.26" />
        <line
          className="cls-3"
          x1="360.76"
          y1="241.12"
          x2="443.6"
          y2="241.12"
        />
        <line
          className="cls-2"
          x1="350.21"
          y1="241.19"
          x2="361.38"
          y2="241.12"
        />
        <line
          className="cls-2"
          x1="352.14"
          y1="214.12"
          x2="357.64"
          y2="214.05"
        />
        <line
          className="cls-2"
          x1="352.12"
          y1="268.36"
          x2="357.62"
          y2="268.29"
        />
        <line
          className="cls-2"
          x1="360.79"
          y1="214.12"
          x2="366.29"
          y2="214.05"
        />
        <line
          className="cls-2"
          x1="360.76"
          y1="268.36"
          x2="366.26"
          y2="268.29"
        />
        <line className="cls-2" x1="261.18" y1="80.33" x2="273.87" y2="80.33" />
        <path
          className="cls-3"
          d="M273.55,80.91c.69-1.15,2.69-4.35,3.38-5.5,1.37,3.07,3,6.54,4.34,9.61,1.35-3.12,2.65-6.3,4-9.37,2.06,4.6,2.59,5.65,4,9.09,1.14-3.25,2.38-5.64,3.53-8.89,1.37,3.25,2.12,6,3.5,9.26.84-1.67,1.35-2.7,2.71-5.35"
        />
        <line className="cls-3" x1="500.27" y1="80.24" x2="543.27" y2="80.24" />
        <line className="cls-3" x1="591.44" y1="80.17" x2="754.78" y2="80.17" />
        <line className="cls-2" x1="580.34" y1="80.24" x2="594.55" y2="80.17" />
        <line
          className="cls-3"
          x1="640.4"
          y1="267.35"
          x2="754.82"
          y2="267.35"
        />
        <line
          className="cls-2"
          x1="632.64"
          y1="267.42"
          x2="642.58"
          y2="267.35"
        />
        <line className="cls-2" x1="543.27" y1="80.24" x2="554.89" y2="80.24" />
        <path
          className="cls-3"
          d="M554.6,80.81c.7-1.15,2.75-4.34,3.46-5.49,1.4,3.06,3,6.54,4.43,9.6,1.38-3.11,2.7-6.3,4-9.36,2.1,4.59,2.65,5.64,4.05,9.09,1.17-3.26,2.44-5.64,3.61-8.89,1.4,3.25,2.17,6,3.57,9.25.86-1.66,1.39-2.69,2.78-5.35"
        />
        <line
          className="cls-2"
          x1="218.75"
          y1="282.86"
          x2="218.65"
          y2="267.09"
        />
        <line
          className="cls-2"
          x1="219.08"
          y1="333.29"
          x2="218.9"
          y2="305.83"
        />
        <polygon
          className="cls-2"
          points="319.79 348.39 312.96 348.39 306.14 348.39 309.55 351.94 312.96 355.49 316.37 351.94 319.79 348.39"
        />
        <line
          className="cls-2"
          x1="313.4"
          y1="348.39"
          x2="313.17"
          y2="313.94"
        />
        <line
          className="cls-2"
          x1="313.4"
          y1="300.54"
          x2="313.18"
          y2="267.38"
        />
        <path
          className="cls-3"
          d="M219.48,306.14c-1.15-.63-4.36-2.47-5.51-3.09,3.05-1.29,6.52-2.79,9.57-4.07-3.12-1.23-6.31-2.41-9.39-3.61,4.58-1.93,5.63-2.43,9.07-3.72-3.26-1-5.65-2.17-8.91-3.21,3.24-1.29,6-2,9.23-3.29l-5.37-2.48"
        />
        <polygon
          className="cls-2"
          points="507.69 360.64 500.87 360.64 494.05 360.64 497.46 364.27 500.87 367.91 504.28 364.27 507.69 360.64"
        />
        <line
          className="cls-2"
          x1="500.97"
          y1="309.02"
          x2="500.87"
          y2="293.64"
        />
        <line
          className="cls-2"
          x1="501.31"
          y1="360.63"
          x2="501.13"
          y2="333.62"
        />
        <path
          className="cls-3"
          d="M501.71,334c-1.16-.68-4.36-2.64-5.52-3.31,3.06-1.38,6.52-3,9.58-4.36-3.13-1.31-6.32-2.58-9.39-3.86,4.58-2.06,5.63-2.6,9.06-4-3.26-1.11-5.65-2.32-8.91-3.43,3.24-1.38,6-2.15,9.23-3.53-1.67-.82-2.7-1.32-5.36-2.65"
        />
        <line
          className="cls-3"
          x1="279.48"
          y1="271.37"
          x2="279.48"
          y2="264.91"
        />
        <line
          className="cls-3"
          x1="276.2"
          y1="268.22"
          x2="282.69"
          y2="268.22"
        />
        <line
          className="cls-3"
          x1="561.26"
          y1="298.63"
          x2="561.26"
          y2="292.17"
        />
        <line
          className="cls-3"
          x1="557.98"
          y1="295.47"
          x2="564.47"
          y2="295.47"
        />
        <line
          className="cls-3"
          x1="276.2"
          y1="217.95"
          x2="282.69"
          y2="217.95"
        />
        <line
          className="cls-3"
          x1="300.11"
          y1="329.16"
          x2="306.59"
          y2="329.16"
        />
        <line
          className="cls-3"
          x1="582.86"
          y1="355.49"
          x2="589.35"
          y2="355.49"
        />
        <line
          className="cls-3"
          x1="393.86"
          y1="167.29"
          x2="400.34"
          y2="167.29"
        />
        <line
          className="cls-3"
          x1="675.59"
          y1="194.72"
          x2="682.07"
          y2="194.72"
        />
        <line
          className="cls-3"
          x1="558.92"
          y1="246.16"
          x2="565.41"
          y2="246.16"
        />
        <line
          className="cls-3"
          x1="303.35"
          y1="288.4"
          x2="303.35"
          y2="281.94"
        />
        <line
          className="cls-3"
          x1="300.07"
          y1="285.25"
          x2="306.56"
          y2="285.25"
        />
        <line
          className="cls-2"
          x1="581.85"
          y1="335.91"
          x2="607.63"
          y2="335.91"
        />
        <line className="cls-2" x1="581.22" y1="327.37" x2={607} y2="327.37" />
        <line
          className="cls-2"
          x1="587.83"
          y1="340.77"
          x2="600.78"
          y2="340.77"
        />
        <line
          className="cls-2"
          x1="587.85"
          y1="331.04"
          x2="600.79"
          y2="331.04"
        />
        <line
          className="cls-2"
          x1="594.55"
          y1="375.22"
          x2="594.32"
          y2="340.77"
        />
        <line
          className="cls-3"
          x1="594.34"
          y1="295.4"
          x2="594.28"
          y2="287.14"
        />
        <line
          className="cls-3"
          x1="313.21"
          y1="269.04"
          x2="313.15"
          y2="260.78"
        />
        <line className="cls-2" x1="594.6" y1="334.44" x2="594.34" y2="295.4" />
        <line className="cls-3" x1="584.5" y1="315.23" x2="584.5" y2="308.77" />
        <line
          className="cls-3"
          x1="581.22"
          y1="312.08"
          x2="587.71"
          y2="312.08"
        />
        <line
          className="cls-2"
          x1="675.76"
          y1="175.82"
          x2="701.54"
          y2="175.82"
        />
        <polygon
          className="cls-2"
          points="600.93 375.22 594.11 375.22 587.29 375.22 590.7 378.77 594.11 382.32 597.52 378.77 600.93 375.22"
        />
        <line
          className="cls-2"
          x1="675.13"
          y1="167.29"
          x2="700.91"
          y2="167.29"
        />
        <line
          className="cls-2"
          x1="681.75"
          y1="180.69"
          x2="694.69"
          y2="180.69"
        />
        <line
          className="cls-2"
          x1="681.76"
          y1="170.96"
          x2="694.7"
          y2="170.96"
        />
        <polygon
          className="cls-2"
          points="694.84 215.13 688.02 215.13 681.2 215.13 684.61 218.69 688.02 222.24 691.43 218.69 694.84 215.13"
        />
        <line
          className="cls-2"
          x1="688.46"
          y1="215.13"
          x2="688.23"
          y2="180.69"
        />
        <line
          className="cls-2"
          x1="688.46"
          y1="167.29"
          x2="688.33"
          y2="147.19"
        />
        <line
          className="cls-3"
          x1="678.41"
          y1="156.33"
          x2="678.41"
          y2="149.87"
        />
        <line
          className="cls-3"
          x1="675.13"
          y1="153.18"
          x2="681.62"
          y2="153.18"
        />
        <line
          className="cls-2"
          x1="394.03"
          y1="148.31"
          x2="419.81"
          y2="148.31"
        />
        <line
          className="cls-2"
          x1="393.4"
          y1="139.77"
          x2="419.18"
          y2="139.77"
        />
        <line
          className="cls-2"
          x1="400.01"
          y1="153.17"
          x2="412.95"
          y2="153.17"
        />
        <line
          className="cls-2"
          x1="400.03"
          y1="143.45"
          x2="412.97"
          y2="143.45"
        />
        <polygon
          className="cls-2"
          points="413.11 187.62 406.29 187.62 399.47 187.62 402.88 191.17 406.29 194.72 409.7 191.17 413.11 187.62"
        />
        <line
          className="cls-2"
          x1="406.73"
          y1="187.62"
          x2="406.5"
          y2="153.17"
        />
        <line
          className="cls-2"
          x1="406.73"
          y1="139.77"
          x2="406.59"
          y2="119.67"
        />
        <line
          className="cls-3"
          x1="396.67"
          y1="128.81"
          x2="396.67"
          y2="122.36"
        />
        <line
          className="cls-3"
          x1="393.4"
          y1="125.66"
          x2="399.88"
          y2="125.66"
        />
        <circle className="cls-4" cx="445.98" cy="241.31" r="2.96" />
        <circle className="cls-4" cx="501.31" cy="241.31" r="2.96" />
        <line
          className="cls-3"
          x1="445.89"
          y1="80.26"
          x2="445.98"
          y2="238.35"
        />
        <line
          className="cls-3"
          x1="205.56"
          y1="80.33"
          x2="205.56"
          y2="212.26"
        />
        <line
          className="cls-3"
          x1="500.88"
          y1="80.23"
          x2="500.97"
          y2="238.32"
        />
        <line className="cls-3" x1="754.2" y1="80.08" x2="754.2" y2="267.38" />
        <line
          className="cls-2"
          x1="541.72"
          y1="241.31"
          x2="554.72"
          y2="241.31"
        />
        <line
          className="cls-3"
          x1="504.27"
          y1="241.31"
          x2="541.72"
          y2="241.31"
        />
        <line
          className="cls-2"
          x1="540.85"
          y1="294.21"
          x2="554.72"
          y2="294.21"
        />
        <line
          className="cls-3"
          x1="500.26"
          y1="294.21"
          x2="540.85"
          y2="294.21"
        />
        <line
          className="cls-2"
          x1="259.32"
          y1="267.09"
          x2="272.55"
          y2="267.09"
        />
        <line
          className="cls-3"
          x1="218.09"
          y1="267.09"
          x2="259.82"
          y2="267.09"
        />
        <line
          className="cls-3"
          x1="688.33"
          y1="147.19"
          x2="594.28"
          y2="147.19"
        />
        <line
          className="cls-3"
          x1="594.28"
          y1="227.4"
          x2="594.28"
          y2="147.36"
        />
        <line
          className="cls-2"
          x1="594.28"
          y1="239.57"
          x2="594.28"
          y2="227.4"
        />
        <line
          className="cls-3"
          x1="594.28"
          y1="247.43"
          x2="594.28"
          y2="239.57"
        />
        <line
          className="cls-3"
          x1="407.12"
          y1="120.27"
          x2="312.35"
          y2="120.27"
        />
        <line
          className="cls-3"
          x1="259.82"
          y1="214.99"
          x2="208.21"
          y2="214.99"
        />
        <line
          className="cls-2"
          x1="272.55"
          y1="214.99"
          x2="259.82"
          y2="214.99"
        />
        <line
          className="cls-3"
          x1="312.35"
          y1="200.48"
          x2="312.35"
          y2="120.44"
        />
        <line
          className="cls-2"
          x1="312.35"
          y1="212.65"
          x2="312.35"
          y2="200.48"
        />
        <line
          className="cls-3"
          x1="312.35"
          y1="221.7"
          x2="312.35"
          y2="212.65"
        />
        <polygon
          className="cls-3"
          points="632.64 267.4 593.79 287.35 554.95 307.29 554.92 267.45 554.9 227.6 593.77 247.5 632.64 267.4"
        />
        <polygon
          className="cls-3"
          points="350.9 241.15 312.06 261.09 273.21 281.04 273.19 241.19 273.16 201.35 312.03 221.25 350.9 241.15"
        />
        <circle className="cls-4" cx="205.56" cy="214.99" r="2.96" />
        <line className="cls-3" x1="325.76" y1="227.6" x2="340.1" y2="213.89" />
        <line
          className="cls-3"
          x1="352.14"
          y1="214.12"
          x2="339.71"
          y2="214.05"
        />
        <line
          className="cls-2"
          x1="633.98"
          y1="240.81"
          x2="639.48"
          y2="240.74"
        />
        <line
          className="cls-2"
          x1="642.63"
          y1="240.81"
          x2="648.13"
          y2="240.74"
        />
        <line
          className="cls-3"
          x1="607.63"
          y1="254.29"
          x2="621.97"
          y2="240.58"
        />
        <line
          className="cls-3"
          x1="633.98"
          y1="240.81"
          x2="621.55"
          y2="240.74"
        />
        <line
          className="cls-3"
          x1="325.25"
          y1="254.25"
          x2="340.17"
          y2="268.52"
        />
        <line
          className="cls-3"
          x1="352.21"
          y1="268.33"
          x2="339.77"
          y2="268.4"
        />
        <line
          className="cls-2"
          x1="633.82"
          y1="294.92"
          x2="639.32"
          y2="294.85"
        />
        <line className="cls-2" x1="642.49" y1="294.96" x2={648} y2="294.89" />
        <line
          className="cls-3"
          x1="606.7"
          y1="280.72"
          x2="621.88"
          y2="295.23"
        />
        <text
          className="cls-5"
          transform="translate(279.47 68.42) scale(1.04 1)"
        >
          R1
        </text>
        <text className="cls-5" transform="translate(173.09 200.78)">
          R2
        </text>
        <text
          className="cls-5"
          transform="translate(466.64 227.1) scale(1.11 1)"
        >
          R4
        </text>
        <text
          className="cls-5"
          transform="translate(562.16 68.42) scale(1.04 1)"
        >
          R3
        </text>
        <text className="cls-5" transform="translate(185.99 305.83)">
          R5
        </text>
        <text className="cls-5" transform="translate(228.42 305.91)">
          10kΩ
        </text>
        <text className="cls-5" transform="translate(272.55 307.77)">
          15V
        </text>
        <text className="cls-5" transform="translate(319.63 276.18)">
          7
        </text>
        <text className="cls-5" transform="translate(300.7 209.24)">
          4
        </text>
        <text className="cls-5" transform="translate(257.92 227.4)">
          2
        </text>
        <text className="cls-5" transform="translate(540.85 253.79)">
          2
        </text>
        <text className="cls-5" transform="translate(359.37 229.76)">
          1
        </text>
        <text className="cls-5" transform="translate(359.8 254.73)">
          6
        </text>
        <text className="cls-5" transform="translate(359.8 282.27)">
          5
        </text>
        <text className="cls-5" transform="translate(641.3 280.65)">
          6
        </text>
        <text className="cls-5" transform="translate(257.02 280.44)">
          3
        </text>
        <text className="cls-5" transform="translate(540.85 307.77)">
          3
        </text>
        <text className="cls-5" transform="translate(325.13 290.62)">
          V2
        </text>
        <text className="cls-5" transform="translate(92.64 228.59)">
          V5
        </text>
        <text className="cls-6" transform="translate(298.76 232.3)">
          <tspan className="cls-7">V</tspan>
          <tspan x="7.09" y={0}>
            -
          </tspan>
        </text>
        <text className="cls-6" transform="translate(298.76 258.41)">
          V+
        </text>
        <text className="cls-8" transform="translate(580.34 259.5)">
          V
          <tspan className="cls-9" x="7.08" y={0}>
            -
          </tspan>
        </text>
        <text className="cls-6" transform="translate(580.34 285.61)">
          V+
        </text>
        <text className="cls-5" transform="translate(89.99 294.4)">
          0
        </text>
        <text className="cls-5" transform="translate(223.54 347.91)">
          0
        </text>
        <text className="cls-5" transform="translate(317.86 363.01)">
          0
        </text>
        <text
          className="cls-5"
          transform="translate(468.01 332.53) scale(0.98 1)"
        >
          R6
        </text>
        <text
          className="cls-5"
          transform="translate(510.64 332.53) scale(0.98 1)"
        >
          10kΩ
        </text>
        <text
          className="cls-5"
          transform="translate(505.76 375.61) scale(0.98 1)"
        >
          0
        </text>
        <text className="cls-5" transform="translate(553.7 335.78)">
          15V
        </text>
        <text className="cls-5" transform="translate(600.93 300.54)">
          7
        </text>
        <text className="cls-5" transform="translate(606.28 317.45)">
          V3
        </text>
        <text className="cls-5" transform="translate(599.01 389.84)">
          0
        </text>
        <text className="cls-5" transform="translate(645.25 172.15)">
          -15V
        </text>
        <text className="cls-5" transform="translate(692.92 229.76)">
          0
        </text>
        <text className="cls-5" transform="translate(700.19 157.36)">
          V4
        </text>
        <text className="cls-5" transform="translate(363.51 144.63)">
          -15V
        </text>
        <text className="cls-5" transform="translate(418.46 129.85)">
          V1
        </text>
        <text className="cls-5" transform="translate(411.18 202.24)">
          0
        </text>
        <text className="cls-5" transform="translate(223.54 206.57)">
          µA741
        </text>
        <text className="cls-5" transform="translate(261.18 293.64)">
          µF1
        </text>
        <text className="cls-5" transform="translate(541.72 319.56)">
          µF2
        </text>
        <text className="cls-5" transform="translate(512.14 234.91)">
          µA741
        </text>
        <text className="cls-5" transform="translate(641.3 255.57)">
          1
        </text>
        <text className="cls-5" transform="translate(641.51 308.65)">
          5
        </text>
        <line className="cls-3" x1="633.91" y1="294.93" x2="621.48" y2={295} />
        <text className="cls-10" transform="translate(318.44 237.65)">
          OS1
        </text>
        <text className="cls-10" transform="translate(318.6 249.57)">
          OS2
        </text>
        <text className="cls-10" transform="translate(599.51 264.26)">
          OS1
        </text>
        <text className="cls-10" transform="translate(599.67 276.18)">
          OS2
        </text>
        <text className="cls-11" transform="translate(88.18 206.57)">
          A
        </text>
        <text className="cls-11" transform="translate(429.79 276.18)">
          B
        </text>
        <text className="cls-11" transform="translate(760.52 283.63)">
          C
        </text>
      </svg>
    </div>
  );
}
