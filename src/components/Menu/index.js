import React, { useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import DataContex from '../../context/DataContext';
import DndMenuVeryHard from '../DragDropTransfer/subcomponents/VeryHard/menu/item';
import DndMenuHard from '../DragDropTransfer/subcomponents/Hard/menu/item';
import {
  productData,
  typesData,
  fabricsData,
  R4Data,
  IAData,
  URData,
  ULData,
  UCData,
  FRData,
  IData,
} from './../../data/features';

import RadioGroupPrimary from './subcomponents/RadioGroupPrimary';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  heading: {
    marginLeft: '20px',
    fontSize: '14px',
    fontWeight: '600',
    margin: '10px 0',
  },

  formControlLabel: {
    '& span': {
      fontSize: '0.8em',
    },
  },
  taskContent: {
    margin: '15px',
  },
}));

const Menu = () => {
  const classes = useStyles();
  const { level } = useContext(DataContex);

  return (
    <div>
      {level === 'easy' && (
        <div className={classes.root}>
          <RadioGroupPrimary
            items={productData}
            keyData="R1"
            title={
              <p>
                Wybierz wartość rezystora R<sub>1</sub> [&#8486;]
              </p>
            }
          />
          <RadioGroupPrimary
            items={typesData}
            keyData="R2"
            title={
              <p>
                Wybierz wartość rezystora R<sub>2</sub> [&#8486;]
              </p>
            }
          />
          <RadioGroupPrimary
            items={fabricsData}
            keyData="R3"
            title={
              <p>
                Wybierz wartość rezystora R<sub>3</sub> [&#8486;]
              </p>
            }
            isNested={true}
          />
          <RadioGroupPrimary
            items={R4Data}
            keyData="R4"
            title={
              <p>
                Wybierz wartość rezystora R<sub>4</sub> [&#8486;]
              </p>
            }
            isNested={true}
          />
        </div>
      )}

      {level === 'average' && (
        <div className={classes.root}>
          <h4 className={classes.taskContent}>
            Wyznacz wartość prądu płynącego oraz spadki napięć w obwodzie dla
            zadanych parametrów na rysunku i częstotliwości źródła 1000Hz.
          </h4>
          <RadioGroupPrimary
            items={IAData}
            keyData="IA"
            title="Wybierz I [mA]"
            isNested={true}
          />
          <RadioGroupPrimary
            items={URData}
            keyData="UR"
            title={
              <p>
                Wybierz U<sub>R</sub> [V]
              </p>
            }
            isNested={true}
          />
          <RadioGroupPrimary
            items={ULData}
            keyData="UL"
            title={
              <p>
                Wybierz U<sub>L</sub> [V]
              </p>
            }
            isNested={true}
          />
          <RadioGroupPrimary
            items={UCData}
            keyData="UC"
            title={
              <p>
                Wybierz U<sub>C</sub> [V]
              </p>
            }
            isNested={true}
          />
          <h4 className={classes.taskContent}>
            Ile wynosi częstotliwość rezonansowa f<sub>r </sub> dla R[&#8486;] =
            1k, L[H] = 1m, C[F] = 1&#956; oraz wartość płynącego prądu (I) w
            stanie rezonansu?
          </h4>
          <RadioGroupPrimary
            items={FRData}
            keyData="FR"
            title={
              <p>
                Wybierz f<sub>r</sub> [Hz]
              </p>
            }
            isNested={true}
          />
          <RadioGroupPrimary
            items={IData}
            keyData="I"
            title="Wybierz I [mA]"
            isNested={true}
          />
        </div>
      )}
      {level === 'hard' && 
      (   <>
        <h4 className={classes.heading}>Przeciągnij element:</h4>
        <DndMenuHard keyData="menu" />
      </>)}
      {level === 'veryHard' && (
        <>
          <h4 className={classes.heading}>Przeciągnij element:</h4>
          <DndMenuVeryHard keyData="menu" />
        </>
      )}
    </div>
  );
};

export default Menu;
