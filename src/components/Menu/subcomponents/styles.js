import { makeStyles } from '@material-ui/core/styles'


const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        fontWeight: theme.typography.fontWeightRegular,
    },

    formControlLabel: {
        '& span': {
            fontSize: '0.8em',
        },
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: "100%"
    },
    subitemTitle: {
        fontSize: 12,
        opacity: "0.7",
        marginTop: "10px"
    },
    bannedWrap: {
        position: "relative",
    },
    banned: {
        width: "100%",
        height: "100%",
        position: "absolute",
        left: "0",
        top: "0",
        zIndex: "999"
    },
    badAnswer: {
        color: "red",
      },
      correctAnswer: {
        color: "green"
      },
}))

export default useStyles;
