import React, { useContext } from "react";
import { useSnackbar } from "notistack";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";

import DataContext from "../../../context/DataContext";
import useStyles from "./styles";
import {allAnswerData} from "../../../data/correctAnswer";

// const useStyles = makeStyles((theme) => ({
//   badAnswer: {
//     color: "red",
//   },
//   correctAnswer: {
//     color: "green"
//   },
  
// }));

const RadioGroupPrimary = ({ title, items, keyData, isNested = false }) => {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();
  const { data, setState, state, level } = useContext(DataContext);
  let { [keyData]: selectedDefault, bannedFeatures } = data;
  const selected = selectedDefault;
  const {isValid} = data;
  const correctAnswer = allAnswerData[`${level}DataCorrect`][keyData]
  const sortedItems = !isNested
  

  
    ? items.filter(
        (item) => item.level.includes("all") || item.level.includes(level)
      )
    : [];

  const updateData = (value) => {
    const newValue =
      value.target.value === selected ? null : value.target.value;
    setState({ ...state, [level]: { ...state[level], [keyData]: newValue } });
  };
  const onClickBanned = () => {
    enqueueSnackbar(
      `Ten element nie może być użyty w tym pomieszczeniu, wybierz inny element z bazy danych`,
      {
        variant: "info",
      }
    );
  };
  return (
    <div>
      <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2a-content"
          id="panel2a-header"
        >
          <Typography className={classes.heading}>{title}</Typography>
        </AccordionSummary>
        <AccordionDetails className={classes.bannedWrap}>
          {bannedFeatures.includes(keyData) && (
            <div onClick={onClickBanned} className={classes.banned}></div>
          )}

          {isNested ? (
            <RadioGroup aria-label="transport" value={selected}>
              {items.map((section) => {
                const { items, title } = section;
                const sortedData = items.filter(
                  (item) =>
                    item.level.includes("all") || item.level.includes(level)
                );
                return (
                  <>
                    <Typography className={classes.subitemTitle}>
                      {title}
                    </Typography>
                    {sortedData.map((item) => (
                      <FormControlLabel
                      className={(isValid&&correctAnswer!=item.key?classes.badAnswer:null)||(isValid&&correctAnswer===item.key?classes.correctAnswer:null)}
                        value={item.key}
                        control={<Radio color="secondary" />}
                        label={item.name}
                        key={item.value}
                        // className={classes.formControlLabel}
                        control={<Radio className={(isValid&&correctAnswer!=item.key?classes.badAnswer:null)||(isValid&&correctAnswer===item.key?classes.correctAnswer:null)}  onClick={updateData} />}
                      />
                    ))}
                  </>
                );
              })}
            </RadioGroup>
          ) : (
            <RadioGroup aria-label="transport" value={selected ? selected : ""}>
              {sortedItems.map((item) => (
                <FormControlLabel
                className={(isValid&&correctAnswer!=item.key?classes.badAnswer:null)||(isValid&&correctAnswer===item.key?classes.correctAnswer:null)}
                  value={item.key}
                  control={<Radio  color="secondary" />}
                  label={item.name}
                  key={item.value}
                  // className={classes.formControlLabel}
                  control={<Radio  className={(isValid&&correctAnswer!=item.key?classes.badAnswer:null)||(isValid&&correctAnswer===item.key?classes.correctAnswer:null)}    onClick={updateData} />}
                />
              ))}
            </RadioGroup>
          )}
        </AccordionDetails>
      </Accordion>
    </div>
  );
};

export default RadioGroupPrimary;
