import React, { useContext, setState } from 'react';
import { Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import DataContext from '../../context/DataContext';
import dataHelper from '../../lib/data';

// import packetProductsData from "@data/packets";
// import labelProductsData from "@data/labels";
// import labelsData from "@data/labels";
// import packetsData from "@data/packets";

import './style.css';

import {
  taskHardCorrectAnswer,
  taskHardPDF,
  taskVeryHardCorrectAnswer,
  taskVeryHardPDF
} from '../../data/features';

const useStyles = makeStyles((theme) => ({
  wrap: {
    fontSize: '30px',
    padding: '10px',
    // position:"absolute",
    // top:"0",
    // left:"0",
    // zIndex:-1
  },
  label: {
    margin: '20px 0 40px 0',
    paddingBottom: '10px',
    borderBottom: '3px solid #1e1c37',
    fontSize: '40px',
  },
  message: {
    color: 'white',
    borderRadius: '10px',
    textAlign: 'center',
    padding: '5px',
    fontSize: '19px',
    // position: "relative",
    // top: "-30px"
  },
  success: {
    backgroundColor: '#28a745',
  },
  error: {
    backgroundColor: '#dc3545',
  },
}));

const Steps = ({ type }) => {
  const classes = useStyles();
  const { state, data, level, hardTaskDnd,veryHardTaskDnd } = useContext(DataContext);

  const renderTable = (number, state, pdf, correct) => {
    return (
      <table id="table-steps">
        <thead>
          <tr>
            <th>Symbol</th>
            <th>Poprawność umiejscowienia</th>
          </tr>
        </thead>
        <tbody>
          {[...Array(number)].map((item, index) => {
            return (
              <tr>
                <td>{pdf[index]}</td>
                <td>
                  {state[`content${index + 1}`].list?.[0]?.text ===
                  correct[`content${index + 1}`]
                    ? 'Poprawna'
                    : 'Błędna'}
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  };

  const { easy, average, hard } = state;

  const { R1, R2, R3, R4 } = easy;
  const { FR, I, IA, UC, UL, UR } = average;

  const isValid = () => {
    let isValid = { easy: false, average: false };

    if (R1 !== '100k' || R2 !== '50k' || R3 !== '150k' || R4 !== '50k') {
    } else {
      isValid.easy = true;
    }

    if (
      IA !== '7,07' ||
      UR !== '7,07' ||
      UL !== '0,044' ||
      UC !== '0,11' ||
      FR !== '5033' ||
      I !== '7,07'
    ) {
    } else {
      isValid.average = true;
    }
    return isValid;
  };

  const valueValid = isValid();

  return (
    <>
      <div style={{ display: 'none', width: '297mm' }} id="pdf-file">
        <div className={classes.wrap}>
          <div className={classes.label}>
            UKŁADY PRACY WZMACNIACZA OPERACYJNEGO
          </div>
          <table id="table-steps">
            <tr>
              <th>Nazwa</th>
              <th>Wartość</th>
            </tr>
            {R1 && (
              <tr>
                <td>
                  <p>
                    Wybierz wartość rezystora R<sub>1</sub> [Ω]
                  </p>
                </td>
                <td>{R1}</td>
              </tr>
            )}

            {R2 && (
              <tr>
                <td>
                  <p>
                    Wybierz wartość rezystora R<sub>2</sub> [Ω]
                  </p>
                </td>
                <td>{R2}</td>
              </tr>
            )}
            {R3 && (
              <tr>
                <td>
                  <p>
                    Wybierz wartość rezystora R<sub>3</sub> [Ω]
                  </p>
                </td>
                <td>{R3}</td>
              </tr>
            )}

            {R4 && (
              <tr>
                <td>
                  <p>
                    Wybierz wartość rezystora R<sub>4</sub> [Ω]
                  </p>
                </td>
                <td>{R4}</td>
              </tr>
            )}
          </table>

          {valueValid.easy ? (
            <div className={`${classes.message} ${classes.success}`}>
              Zadanie wykonane prawidłowo
            </div>
          ) : (
            <div className={`${classes.message} ${classes.error}`}>
              Zadanie wykonane nieprawidłowo
            </div>
          )}

          <div className={classes.label}>UKŁAD RLC</div>
          <span style={{ fontSize: 17 }}>
            Wartość prądu płynącego oraz spadki napięć w obwodzie dla zadanych
            parametrów na rysunku i częstotliwości źródła 1000Hz.
          </span>
          <table id="table-steps">
            <tr>
              <th>Nazwa</th>
              <th>Wartość</th>
            </tr>
            {IA && (
              <tr>
                <td>
                  <p>I [A]</p>
                </td>
                <td>{IA}</td>
              </tr>
            )}

            {UR && (
              <tr>
                <td>
                  <p>
                    U<sub>R</sub> [V]
                  </p>
                </td>
                <td>{UR}</td>
              </tr>
            )}
            {UL && (
              <tr>
                <td>
                  <p>
                    U<sub>L</sub> [V]
                  </p>
                </td>
                <td>{UL}</td>
              </tr>
            )}

            {UC && (
              <tr>
                <td>
                  <p>
                    U<sub>C</sub> [V]
                  </p>
                </td>
                <td>{UC}</td>
              </tr>
            )}
          </table>

          <span style={{ fontSize: 17 }}>
            Częstotliwość rezonansowa f<sub>r </sub> dla R[Ω] = 1k, L[H] = 1m,
            C[F] = 1μ oraz wartość płynącego prądu (I) w stanie rezonansu
          </span>
          <table id="table-steps">
            <tr>
              <th>Nazwa</th>
              <th>Wartość</th>
            </tr>
            {FR && (
              <tr>
                <td>
                  <p>
                    f<sub>r</sub> [Hz]
                  </p>
                </td>
                <td>{FR}</td>
              </tr>
            )}

            {I && (
              <tr>
                <td>Wybierz I [A]</td>
                <td>{I}</td>
              </tr>
            )}
          </table>

          {valueValid.average ? (
            <div className={`${classes.message} ${classes.success}`}>
              Zadanie wykonane prawidłowo
            </div>
          ) : (
            <div className={`${classes.message} ${classes.error}`}>
              Zadanie wykonane nieprawidłowo
            </div>
          )}

          <div className={classes.label}>Budowanie układu</div>
          {renderTable(17, hardTaskDnd, taskHardPDF, taskHardCorrectAnswer)}

          {state.hard.isValid ? (
            <div className={`${classes.message} ${classes.success}`}>
              Zadanie wykonane prawidłowo
            </div>
          ) : (
            <div className={`${classes.message} ${classes.error}`}>
              Zadanie wykonane nieprawidłowo
            </div>
          )}

          <div className={classes.label}>Eksperyment w domu </div>
          {renderTable(2, veryHardTaskDnd, taskVeryHardPDF, taskVeryHardCorrectAnswer)}

          {state.veryHard.isValid ? (
            <div className={`${classes.message} ${classes.success}`}>
              Zadanie wykonane prawidłowo
            </div>
          ) : (
            <div className={`${classes.message} ${classes.error}`}>
              Zadanie wykonane nieprawidłowo
            </div>
          )}
        </div>
      </div>
    </>
  );
};

export default Steps;
