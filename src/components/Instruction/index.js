import React, { useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AudioPlayer from '../AudioPlayer';

import ModalContext from '@context/ModalContext';
import DataContext from '@context/DataContext';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';

import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';

// import polecenie1 from "./../../sounds/polecenie1.mp3";
// import polecenie2 from "./../../sounds/polecenie2.mp3";
import wykres1 from '../../components/Content/subcomponents/img/wykres1.svg';
import wykres2 from '../../components/Content/subcomponents/img/wykres2.svg';
import wykres3 from '../../components/Content/subcomponents/img/wykres3.svg';
import wykres4 from '../../data/img/schema2.png';

import command1 from '../../data/lector/command1.mp3';
import command2 from '../../data/lector/command2.mp3';
import command3_1 from '../../data/lector/command3.1.mp3';
import command3_2 from '../../data/lector/command3.2.mp3';

const useStyles = makeStyles((theme) => ({
  backDropSuccess: {
    background: 'rgba(65,172,38,0.3)',
  },
  backDrop: {},
  icon: {
    textAlign: 'center',
    marginBottom: '10px',
    width: '100%',
  },
  iconImg: {
    margin: '0 auto',
  },
  title: {
    textAlign: 'center',
    position: 'relative',
    maxWidth: '100%',
    marginBottom: '20px',
    color: '#595959',
    fontSize: '25px',
    fontWeight: '600',
    textAlign: 'center',
    textTransform: 'none',
    wordWrap: 'break-word',
  },
  description: {
    color: '#595959',
    fontWeight: '600',
    textAlign: 'center',
    fontSize: '16px',
    marginBottom: '20px',
  },
  buttonsWrap: {
    display: 'flex',
    justifyContent: 'center',
  },
  button: {
    margin: '10px',
  },
}));

const Instruction = () => {
  const { isInstructionOpen, setIsInstructionOpen } = useContext(ModalContext);
  const { level, getImagePath } = useContext(DataContext);
  const classes = useStyles();

  const handleClose = () => {
    setIsInstructionOpen(false);
  };
  return (
    <>
      <Dialog
        maxWidth={level === 'easy' ? 'md' : 'sm'}
        fullWidth
        open={isInstructionOpen}
        onClose={handleClose}
        style={{ zIndex: '999999' }}
      >
        <DialogContent style={{ padding: '25px 25px 25px 25px' }}>
          <h4
            style={{
              marginBottom: '20px',
              fontSize: '25px',
              textAlign: 'center',
            }}
          >
            Polecenie
          </h4>
          <div style={{ position: 'absolute', top: '15px', right: '15px' }}>
            <IconButton
              onClick={() => setIsInstructionOpen(false)}
              aria-label="Zamknij"
            >
              <CloseIcon />
            </IconButton>
          </div>

          <div className={classes.instruction}>
            {level === 'easy' && (
              <>
                <p>
                  Dobierz wartości rezystorów R1, R2, R3, R4 aby w punktach B i
                  C otrzymać napięcia o podanych przebiegach:
                </p>
                <AudioPlayer url={command1} />
                <span>- Punkt A</span>
                <img  crossOrigin='anonymous'src={getImagePath(wykres1)} alt="wykres1" />
                <span>- Punkt B</span>
                <img crossOrigin='anonymous' src={getImagePath(wykres2)} alt="wykres2" />
                <span>- Punkt C</span>
                <img  crossOrigin='anonymous'src={getImagePath(wykres3)} alt="wykres3" />
              </>
            )}
            {level === 'average' && (
              <>
                <p>
                  Wyznacz wartość prądu płynącego oraz spadki napięć w obwodzie
                  dla zadanych parametrów na rysunku i częstotliwości źródła
                  1000Hz. Ile wynosi częstotliwość rezonansowa dla zadanych
                  parametrów oraz wartość płynącego prądu w stanie rezonansu?
                </p>
                <AudioPlayer url={command2} />
              </>
            )}
            {level === 'hard' && (
              <div style={{ marginTop: '30px' }}>
                <p>I etap</p>
                <p>
                  Na podstawie poniższego schematu prostego, zbuduj układ
                  składający się z następujących elementów:
                </p>
                <ul>
                  <li>1 × komparator typu LM311,</li>
                  <li>1 × termistor,</li>
                  <li>1 × kondensator o pojemności 100 nF,</li>
                  <li>1 × kondensator o pojemności 220 μF,</li>
                  <li>1 × rezystor o wartości 330 Ω,</li>
                  <li>2 × rezystor o wartości 1 kΩ,</li>
                  <li>1 × rezystor o wartości 56 kΩ,</li>
                  <li>1 × rezystor o wartości 1 MΩ,</li>
                  <li>
                    1 × potencjometr o wartości maksymalnej opornosci 5 kΩ,
                  </li>
                  <li>1 × dioda świecąca,</li>
                  <li>1 × płytka stykowa.</li>
                </ul>
                <AudioPlayer url={command3_1} />
                <p>II etap</p>
                <p>
                  Na podstawie poniższego schematu prostego połącz elementy z
                  masą układu wykorzystując komplet przewodów połączeniowych.
                </p>
                <AudioPlayer url={command3_2} />

                <img crossOrigin='anonymous' src={getImagePath(wykres4)} />
              </div>
            )}
          </div>
        </DialogContent>
      </Dialog>
    </>
  );
};

export default Instruction;
