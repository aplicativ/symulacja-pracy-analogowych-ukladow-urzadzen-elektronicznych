import React, { useContext, useState } from "react";
import { Button } from "@material-ui/core";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";

import ModalContext from "@context/ModalContext";
import DataContext from "@context/DataContext";

import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";

import successIcon from "./icons/success.jpg";
import errorIcon from "./icons/error.png";
import warningIcon from "./icons/info.jpg";

import plytka from "./img/plytka.png";

import CloseIcon from "@mui/icons-material/Close";
import IconButton from "@mui/material/IconButton";

import printSteps from "./../../lib/pdf";

const useStyles = makeStyles((theme) => ({
  backDropSuccess: {
    background: "rgba(65,172,38,0.3)",
  },
  backDrop: {},
  icon: {
    textAlign: "center",
    marginBottom: "10px",
    width: "100%",
  },
  iconImg: {
    margin: "0 auto",
  },
  title: {
    textAlign: "center",
    position: "relative",
    maxWidth: "100%",
    marginBottom: "20px",
    color: "#595959",
    fontSize: "25px",
    fontWeight: "600",
    textTransform: "none",
    wordWrap: "break-word",
  },
  description: {
    color: "#595959",
    textAlign: "left",
    fontSize: "16px",
    marginBottom: "20px",
  },
  buttonsWrap: {
    display: "flex",
    justifyContent: "center",
  },
  button: {
    margin: "10px",
  },
  close: {
    position: "absolute",
    top: "15px",
    right: "15px",
  },
  alignCenterDescription: {
    textAlign: "center",
  },
}));

const Modal = () => {
  let { modalParams, setModalParams } = useContext(ModalContext);
  let { getImagePath, level } = useContext(DataContext);
  const { type, title, text, isOpen } = modalParams;

  const [finishTask, setFinishTask] = useState(null);

  const classes = useStyles();

  const handleClose = () => {
    setModalParams({
      ...modalParams,
      isOpen: false,
      type: null,
      title: null,
      text: null,
    });
    setFinishTask(null);
  };

  const onClickTask = (task) => {
    setFinishTask(task);
  };

  const task = () => {
    return {
      __html: `W ramach pracy własnej lub pracowni wykonaj podane ćwiczenia. W tym celu możesz wykorzystać elementy elektroniczne, przyrządy pomiarowe i płytkę stykową.</br>
                <ul><b>Propozycje ćwiczeń: </b><li>wyznaczyć wzmocnienie napięcia na poszczególnych stopniach wzmacniacza (uwaga: wartości rezystancji stanowią jedynie propozycję i mogą być zmienione dla potrzeb ćwiczenia, np. dobrane, aby uzyskać określone wzmocnienie),</li><li>dokonać pomiaru napięć na wyjściach poszczególnych wzmacniaczy operacyjnych i porównać przebieg z przebiegiem napięcia wejściowego,</li><li>zmodyfikować układ przez zastosowanie na poszczególnych stopniach innych układów pracy (np. nieodwracającego, całkującego czy różniczkującego) oraz zbadać wpływ parametrów rezystancji i pojemności na przebiegi,</li><li>wykonać pomiar przebiegów napięć dla układów różniczkujących przy wymuszeniu przebiegiem prostokątnym.</li></ul>`,
    };
  };

  const taskRLC = () => {
    return {
      __html: `
                W  ramach  pracy własnej lub pracowni wykonaj podane ćwiczenia.  W  tym  celu możesz wykorzystać elementy elektroniczne, przyrządy pomiarowe i płytkę stykową.</br>
                <ul><b>Propozycje ćwiczeń: </b><li>korzystając  z  przyrządów  pomiarowych  (multimetr,  mostek  RLC)  wykonać  pomiar  kilku wartości rezystancji, pojemności i indukcyjności wykorzystywanych w ćwiczeniu,</li><li>dokonać pomiaru  napięć   na   poszczególnych   elementach   dla   amplitudy  napięcia źródła   10V   i częstotliwości 50Hz, 500Hz, 2000Hz,</li><li>wyznaczyć wartość prądu w obwodzie dla powyższych pomiarów,</li><li>wyznaczyć  częstotliwości   rezonansowe   na   podstawie   pomiaru   spadku  napięcia   na rezystorze oraz wyznaczyć charakterystyki częstotliwościowe,</li><li>wyznaczyć częstotliwości rezonansowe dla innych wartości parametrów RLC,</li><li>zmodyfikować układ i powtórzyć pomiary dla układów równoległego RLC oraz połączenia mieszanego,</li><li>dobrać parametry RLC w celu uzyskania częstotliwości rezonansowych, np. 5kHz.</li></ul>`,
    };
  };

  return (
    <Dialog
      fullWidth
      open={isOpen}
      onClose={handleClose}
      style={{ zIndex: "999999" }}
      BackdropProps={{
        classes: {
          root: type == "success" ? classes.backDropSuccess : classes.backDrop,
        },
      }}
    >
      <DialogContent style={{ padding: "25px" }}>
        <div className={classes.close}>
          <IconButton onClick={handleClose} aria-label="close" component="span">
            <CloseIcon
              style={{
                stroke: "#757575",
                strokeWidth: 2,
              }}
            />
          </IconButton>
        </div>
        {!finishTask && (
          <>
            {type === "success" && (
              <div className={classes.icon}>
                <img crossOrigin='anonymous' className={classes.iconImg} src={successIcon} />
              </div>
            )}
            {type === "error" && (
              <div className={classes.icon}>
                <img crossOrigin='anonymous' className={classes.iconImg} src={errorIcon} />
              </div>
            )}
            {type === "warning" && (
              <div className={classes.icon}>
                <img crossOrigin='anonymous' className={classes.iconImg} src={warningIcon} />
              </div>
            )}

            {title && <div className={classes.title}>{title}</div>}
            {text && <p className={type === "warning" || type === "success" ? classes.alignCenterDescription : classes.description}>{text}</p>}
          </>
        )}

        {finishTask == "easy" && (
          <>
            <img style={{ maxWidth: "100%" }} src={getImagePath(plytka)} />
            <div dangerouslySetInnerHTML={task()} />
          </>
        )}

        {finishTask == "average" && (
          <>
            <img  crossOrigin='anonymous' style={{ maxWidth: "100%" }} src={getImagePath(plytka)} />
            <div dangerouslySetInnerHTML={taskRLC()} />
          </>
        )}
        {type === "error" && (
          <ul style={{ textAlign: "left", fontSize: 15 }}>
            <li>
              {" "}
              <a className="link" aria-label="Przejdź do e-booka" target="_blank" href="https://edytor.zpe.gov.pl/a/Derb8l5kQ">
                Przejdź do e-booka
              </a>
            </li>
          </ul>
        )}

        <div className={classes.buttonsWrap}>
          {type === "error" && (
            <>
              <Button
                onClick={() => printSteps()}
                variant="contained"
                className={classes.button}
                style={{ backgroundColor: "#d9d9d9", color: "black" }}
              >
                Pobierz listę kroków
              </Button>
              <Button onClick={handleClose} variant="contained" className={classes.button} style={{ backgroundColor: "#1979c2", color: "#FFFFFF" }}>
                Spróbuj ponownie
              </Button>
            </>
          )}

          {type === "success" && (
            <>
              <Button onClick={handleClose} variant="contained" className={classes.button} style={{ backgroundColor: "#d9d9d9", color: "black" }}>
                {finishTask ? "Ok" : "Wróć"}
              </Button>
              {!finishTask && (level === 'easy' || level === 'average') &&(
                <Button
                  onClick={() => onClickTask(level)}
                  variant="contained"
                  className={classes.button}
                  style={{ backgroundColor: "#357a38", color: "#FFFFFF" }}
                >
                  Zadanie dodatkowe
                </Button>
              )}
              <Button
                onClick={() => printSteps()}
                variant="contained"
                className={classes.button}
                style={{ backgroundColor: "#1979c2", color: "#FFFFFF" }}
              >
                Pobierz listę kroków
              </Button>
            </>
          )}

          {type === "warning" && (
            <Button onClick={handleClose} variant="contained" className={classes.button} style={{ backgroundColor: "#1979c2", color: "#FFFFFF" }}>
              Powrót
            </Button>
          )}
        </div>
      </DialogContent>
    </Dialog>
  );
};

export default Modal;
