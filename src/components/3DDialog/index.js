import React, { useContext, useState } from "react";

import DialogContent from "@material-ui/core/DialogContent";
import { DialogActions } from "@material-ui/core";
import Button from "@mui/material/Button";
import BootstrapDialogTitle, { BootstrapDialog } from "../Dialog";
import DataContext from "../../context/DataContext";

export default function Index({ children, title }) {
  const [open, setOpen] = useState(false);
  const { level, getImagePath } = useContext(DataContext);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleCloseDialog = () => {
    setOpen(false);
  };
  return (
    <>
      <Button variant="contained" onClick={handleClickOpen}>
        Podgląd 3D
      </Button>
      <BootstrapDialog
        onClose={handleCloseDialog}
        aria-labelledby="customized-dialog-title"
        open={open}
        fullScreen
        style={{ zIndex: "999999" }}
      >
        <BootstrapDialogTitle
          id="customized-dialog-title"
          onClose={handleCloseDialog}
        >
          {title}
        </BootstrapDialogTitle>
        <DialogContent
          dividers
          style={{
            backgroundColor: "#cefecc",
            display: "flex",
            justifyContent: "center",
          }}
        >
          {children}
        </DialogContent>
        <DialogActions></DialogActions>
      </BootstrapDialog>
    </>
  );
}
