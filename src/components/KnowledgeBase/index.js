import React, { useState, useContext } from 'react';

// Components and mui
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';

import { Typography } from '@material-ui/core';

import CloseIcon from '@mui/icons-material/Close';
import IconButton from '@mui/material/IconButton';

import ModalContext from '../../context/ModalContext';
// Style
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  inner: {
    padding: '30px',
  },
  title: {
    marginBottom: '20px',
    fontSize: '25px',
    textAlign: 'center',
  },
  description: {
    marginBottom: '20px',
    fontSize: 16,
    textAlign: 'left',
  },
  close: {
    position: 'absolute',
    top: '10px',
    right: '10px',
  },
  closeIcon: {
    stroke: '#757575',
    strokeWidth: 2,
  },
  tabLabel: {
    fontSize: '18px',
    width: '210px',
  },

});

const KnowledgeBase = () => {
  const { isKnowledgeBaseOpen, setIsKnowledgeBaseOpen } =
    useContext(ModalContext);
  const classes = useStyles();

  const handleClose = () => {
    setIsKnowledgeBaseOpen(false);
  };

  return (
    <Dialog
      style={{ zIndex: '999999', fontSize: '18px' }}
      maxWidth="lg"
      fullWidth
      onClose={handleClose}
      aria-labelledby="simple-dialog-title"
      open={isKnowledgeBaseOpen}
    >
      <DialogContent className={classes.inner}>
        <div className={classes.close}>
          <IconButton onClick={handleClose} aria-label="close" component="span">
            <CloseIcon
              style={{
                stroke: '#757575',
                strokeWidth: 2,
              }}
            />
          </IconButton>
        </div>
        <ul>
          <li>
            <a
              className={classes.underlineoff}
              href="https://edytor.zpe.gov.pl/x/D1FlqQb9u?lang=pl#obwod-rlc"
            >
              Obwód RLC
            </a>
          </li>
          <li>
            <a
              className={classes.underlineoff}
              href="https://edytor.zpe.gov.pl/x/D1FlqQb9u?lang=pl#wtornik-emiterowy"
            >
              Wtórnik emiterowy
            </a>
          </li>
          <li>
            <a
              className={classes.underlineoff}
              href="https://edytor.zpe.gov.pl/x/D1FlqQb9u?lang=pl#wzmacniacze-z-tranzystorem-bipolarnym"
            >
              Wzmacniacze z tranzystorem bipolarnym
            </a>
          </li>
          <li>
            <a
              className={classes.underlineoff}
              href="https://edytor.zpe.gov.pl/x/D1FlqQb9u?lang=pl#wzmacniacz-operacyjny---uklady-pracy"
            >
              Wzmacniacz operacyjny - układ pracy
            </a>
          </li>
          <li>
            <a
              className={classes.underlineoff}
              href="https://edytor.zpe.gov.pl/x/D1FlqQb9u?lang=pl#filtry-sygnalowe"
            >
              Filtry sygnałowe
            </a>
          </li>
          <li>
            <a
              className={classes.underlineoff}
              href="https://edytor.zpe.gov.pl/x/D1FlqQb9u?lang=pl#generatory-sygnalow"
            >
              Generatory sygnałów
            </a>
          </li>
          <li>
            <a
              className={classes.underlineoff}
              href="https://edytor.zpe.gov.pl/x/D1FlqQb9u?lang=pl#prostowniki-jednofazowe"
            >
              Prostowniki jednofazowe
            </a>
          </li>
          <li>
            <a
              className={classes.underlineoff}
              href="https://edytor.zpe.gov.pl/x/D1FlqQb9u?lang=pl#stabilizatory-napiecia-i-zasilacze"
            >
              Stabilizatory napięcia i zasilacze
            </a>
          </li>
          <li>
            <a
              className={classes.underlineoff}
              href="https://edytor.zpe.gov.pl/x/D1FlqQb9u?lang=pl#komparatory-sygnalow"
            >
              Komparatory sygnałów
            </a>
          </li>
        </ul>
      </DialogContent>
    </Dialog>
  );
};

export default KnowledgeBase;
