import React, { useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import DataContex from '../../context/DataContext';
import ModalContext from '@context/ModalContext';
import { Button } from '@material-ui/core';
import Tooltip from '@material-ui/core/Tooltip';
import success from '../../data/audio/good.mp3';
import error from '../../data/audio/bad.mp3';
import {
  taskVeryHardCorrectAnswer,
  taskHardCorrectAnswer,
} from '../../data/features';

import Box from '@mui/material/Box';

const useStyles = makeStyles((theme) => ({
  tooltip: {
    fontSize: '12px',
  },
}));

const Validation = () => {
  const classes = useStyles();
  const { data, level, setState, state, getImagePath, columnsInTask } =
    useContext(DataContex);
  const audioSuccess = new Audio(getImagePath(success));
  const audioError = new Audio(getImagePath(error));

  let { setModalParams } = useContext(ModalContext);
  const initModal = ({ type, title, text }) => {
    setModalParams({ type, title, text, isOpen: true });
  };

  const checkCorrectAnswer = (start, numberOfItems, correctAnswer) => {
    let check = true;

    for (let index = start; index <= numberOfItems; index++) {
      if (columnsInTask[`content${index}`].list.length === 0) {
        check = false;
      } else {
        if (
          columnsInTask[`content${index}`].list[0].text !==
          correctAnswer[`content${index}`]
        ) {
          check = false;
        }
      }
    }
    return check;
  };

  const isValid = () => {
    let isValid = false;
    const { R1, R2, R3, R4, IA, UR, UL, UC, FR, I } = data;
    const selected = [R1, R2, R3, R4];
    const selectedRLC = [IA, UR, UL, UC, FR, I];

    if (level === 'easy' && !selected.every((value) => value != null)) {
      // message.warning("Nie wybrałeś wszystkich wartości rezystorów.");
      initModal({
        type: 'warning',
        title: 'Niestety...',
        text: 'Nie wybrałeś wszystkich wartości rezystorów.',
      });
      audioError.play();
    } else if (
      level === 'average' &&
      !selectedRLC.every((value) => value != null)
    ) {
      // message.warning("Nie wybrałeś wszystkich wartości.");
      initModal({
        type: 'warning',
        title: 'Niestety...',
        text: 'Nie wybrałeś wszystkich wartości.',
      });
      audioError.play();
    } else {
      if (
        level === 'easy' &&
        (R1 !== '100k' || R2 !== '50k' || R3 !== '150k' || R4 !== '50k')
      ) {
        audioError.play();
        // message.error(
        //   "Pomyłka!",
        //   "Jeśli miałeś trudności z wykonaniem tego zadania, wróć do e-booka i wyszukaj w nim informacje o układach pracy wzmacniaczy operacyjnych. Przeanalizuj je ponownie. Dzięki temu następnym razem na pewno prawidłowo dobierzesz wartości rezystorów. Powodzenia!"
        // );
        setState({ ...state, [level]: { ...state[level], isValid: true } });
        initModal({
          type: 'error',
          title: 'Niestety...',
          text: 'Jeśli miałeś trudności z wykonaniem tego zadania, wróć do e-booka i wyszukaj w nim informacje o układach pracy wzmacniaczy operacyjnych. Przeanalizuj je ponownie. Dzięki temu następnym razem na pewno prawidłowo dobierzesz wartości rezystorów. Powodzenia!',
        });
      } else if (
        level === 'average' &&
        (IA !== '7,07' ||
          UR !== '7,07' ||
          UL !== '0,044' ||
          UC !== '0,11' ||
          FR !== '5033' ||
          I !== '7,07')
      ) {
        audioError.play();
        // message.error(
        //   "Pomyłka!",

        //   "Jeśli miałeś trudności z wykonaniem tych zadań, przeanalizuj ponownie materiały dotyczące obwodów prądu zmiennego RLC. Znajdziesz je w animacjach w 2D/3D."
        // );
        setState({ ...state, [level]: { ...state[level], isValid: true } });
        initModal({
          type: 'error',
          title: 'Niestety...',
          text: 'Jeśli miałeś trudności z wykonaniem tych zadań, przeanalizuj ponownie materiały dotyczące obwodów prądu zmiennego RLC. Znajdziesz je w animacjach w 2D/3D.',
        });
      } else {
        audioSuccess.play();
        isValid = true;
      }
    }
    return isValid;
  };

  const onClick = () => {
    // initModal({ type: "success", title: "Gratulacje!", text: "Ćwiczenie jest poprawnie rozwiązane poprawnie!" });

    if (level === 'easy' && isValid()) {
      // message.success(`Ćwiczenie jest poprawnie rozwiązane poprawnie!`);
      initModal({
        type: 'success',
        title: 'Gratulacje!',
        text: 'Bardzo dobrze wykonałeś ćwiczenia.',
      });
    }
    if (level === 'average' && isValid()) {
      // messageRLC.success(`Ćwiczenie jest poprawnie rozwiązane poprawnie!`);
      initModal({
        type: 'success',
        title: 'Brawo!',
        text: 'Bardzo dobrze wykonałeś ćwiczenia.',
      });
    }

    if (level === 'hard') {
      if (data.step === 1) {
        let check = checkCorrectAnswer(1, 11, taskHardCorrectAnswer);
        if (check) {
          initModal({
            type: 'success',
            title: 'Brawo!',
            text: 'Poprawna odpowiedź! Na podstawie schematu jesteś w stanie zbudować układ.',
          });
          setState({ ...state, [level]: { ...state[level], isValid: true } });
          audioSuccess.play();
        } else {
          initModal({
            type: 'error',
            title: 'Niestety...',
            text: 'Niestety, to nie jest prawidłowe rozwiązanie. Jeżeli wykonanie tego zadania sprawiło ci trudność, zapoznaj się ponownie ze schematem umieszczonym w treści polecenia do tego zadania oraz skorzystaj z podglądu 3D złożonego układu. Następnie spróbuj wykonać ćwiczenie jeszcze raz.',
          });
          setState({ ...state, [level]: { ...state[level], isValid: false } });
          audioError.play();
        }
      } else if (data.step === 2) {
        let check = checkCorrectAnswer(12, 17, taskHardCorrectAnswer);
        if (check) {
          initModal({
            type: 'success',
            title: 'Brawo!',
            text: 'Poprawna odpowiedź! Na podstawie schematu jesteś w stanie zbudować układ.',
          });
          setState({ ...state, [level]: { ...state[level], isValid: true } });
          audioSuccess.play();
        } else {
          initModal({
            type: 'error',
            title: 'Niestety...',
            text: 'Niestety, to nie jest prawidłowe rozwiązanie. Jeżeli wykonanie tego zadania sprawiło ci trudność, zapoznaj się ponownie ze schematem umieszczonym w treści polecenia do tego zadania oraz skorzystaj z podglądu 3D złożonego układu. Następnie spróbuj wykonać ćwiczenie jeszcze raz.',
          });
          setState({ ...state, [level]: { ...state[level], isValid: false } });
          audioError.play();
        }
      }
    }

    if (level === 'veryHard') {
      let check = checkCorrectAnswer(1, 2, taskVeryHardCorrectAnswer);

      if (check) {
        initModal({
          type: 'success',
          title: 'Brawo!',
          text: 'Poprawna odpowiedź! Wniosek został poprawnie uzupełniony.',
        });
        setState({ ...state, [level]: { ...state[level], isValid: true } });
        audioSuccess.play();
      } else {
        initModal({
          type: 'error',
          title: 'Niestety...',
          text: 'Niestety, to nie jest prawidłowe rozwiązanie.',
        });
        setState({ ...state, [level]: { ...state[level], isValid: false } });
        audioError.play();
      }
    }
  };

  return (
    <Box
      style={{ width: '100%', display: 'flex', justifyContent: 'center' }}
      justify="center"
    >
      <Tooltip
        title={
          <span className={classes.tooltip}>
            Kliknij, aby sprawdzić, czy dokonałeś poprawnego wyboru
          </span>
        }
        arrow={true}
        placement={'top'}
      >
        <Button
          onClick={onClick}
          className={classes.contentInfoButton}
          variant="contained"
          color="primary"
        >
          Sprawdź
        </Button>
      </Tooltip>
    </Box>
  );
};
export default Validation;
