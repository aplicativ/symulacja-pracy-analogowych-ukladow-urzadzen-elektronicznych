import React, { useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import DataContex from '../../../context/DataContext';
import Resistor from '@components/Resistor';

import { TransformWrapper, TransformComponent } from 'react-zoom-pan-pinch';
import RotateLeftIcon from '@material-ui/icons/RotateLeft';
import AudioPlayer from '../../AudioPlayer';

import command1 from '../../../data/lector/command1.1.mp3';

import Fab from '@material-ui/core/Fab';
import Add from '@material-ui/icons/Add';
import Tooltip from '@material-ui/core/Tooltip';
import Remove from '@material-ui/icons/Remove';

const useStyles = makeStyles({
  image: {
    width: '100%',
    maxWidth: '300px',
  },

  mainDiv: {
    // margin: "30px",
  },

  comments: {
    color: '#1e1c37',
  },
  task: {
    textAlign: 'center',
  },

  tools: {
    display: 'inline-flex',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    margin: '120px 0 40px 0',
    position: 'absolute',
    left: 10,
    top: 0,
    '& button': {
      margin: 5,
    },
  },
  zoomAndPicture: {
    display: 'inline',
  },
  tooltip: {
    fontSize: '12px',
  },
  content1: {
    width: '100%',
    // float: "right",
    maxWidth: 900,
  },
});

const initialOptions = {
  centerContetent: false,
  limitToBounds: false,
  limitToWrapper: true,
};

const Easy = () => {
  const classes = useStyles();

  const { level, data } = useContext(DataContex);

  return (
    <div className={classes.mainDiv}>
      {level === 'easy' && (
        <>
          <div
            style={{
              border: '5px dashed #1e1c37',
              position: 'relative',
              top: 0,
              left: -60,
              paddingLeft: 70,
              width: '1000px',
            }}
          >
            <TransformWrapper options={initialOptions}>
              {({ zoomIn, zoomOut, resetTransform, ...rest }) => (
                <div className={classes.zoomAndPicture}>
                  <div className={classes.tools}>
                    <Tooltip
                      title={
                        <span className={classes.tooltip}>
                          Kliknij, aby powiększyć widok
                        </span>
                      }
                      arrow={true}
                      placement={'top'}
                    >
                      <Fab
                        color="primary"
                        aria-label="Powiększ"
                        onClick={() => zoomIn()}
                      >
                        <Add />
                      </Fab>
                    </Tooltip>
                    <Tooltip
                      title={
                        <span className={classes.tooltip}>
                          Kliknij, aby zmniejszyć widok
                        </span>
                      }
                      arrow={true}
                      placement={'top'}
                    >
                      <Fab onClick={() => zoomOut()}>
                        <Remove />
                      </Fab>
                    </Tooltip>
                    <Tooltip
                      title={
                        <span className={classes.tooltip}>
                          Powrót do podstawowego widoku
                        </span>
                      }
                      arrow={true}
                      placement={'top'}
                    >
                      <Fab onClick={() => resetTransform()}>
                        <RotateLeftIcon fontSize="small" />
                      </Fab>
                    </Tooltip>
                  </div>
                  <div className={classes.pnzContainer}>
                    <TransformComponent>
                      <Resistor {...data} />
                    </TransformComponent>
                  </div>
                </div>
              )}
            </TransformWrapper>
          </div>
          <br /> <br />
          <div className={classes.content1}>
            <h3 className={classes.comments}>Uwagi do układu:</h3>
            <ul>
              <li>
                zastosowano wzmacniacze operacyjne uA741 - napięcie zasilania
                +15VDC, możliwe jest zastosowanie innych wzmacniaczy
                operacyjnych,
              </li>
              <li>układ stanowi złożenie dwóch układów odwracających,</li>
              <li>
                źródło V5 jest źródłem napięcia sinusoidalnego o amplitudzie
                napięcia 1V i częstotliwości 50Hz.
              </li>
            </ul>
            <AudioPlayer url={command1} />
          </div>
        </>
      )}
    </div>
  );
};

export default Easy;
