import React, { useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import DataContex from '../../../context/DataContext';
import RLC from '@components/RLC';
import { TransformWrapper, TransformComponent } from 'react-zoom-pan-pinch';
import RotateLeftIcon from '@material-ui/icons/RotateLeft';
import AudioPlayer from '../../AudioPlayer';
import command2 from '../../../data/lector/command2.2.mp3';
import Fab from '@material-ui/core/Fab';
import Add from '@material-ui/icons/Add';
import Remove from '@material-ui/icons/Remove';

const useStyles = makeStyles({
  image: {
    width: '100%',
    maxWidth: '300px',
  },

  comments: {
    color: '#1e1c37',
  },
  task: {
    textAlign: 'center',
  },

  tools: {
    display: 'inline-flex',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    margin: '120px 0 40px 0',
    position: 'absolute',
    left: 10,
    top: 0,
    '& button': {
      margin: 5,
    },
  },
  zoomAndPicture: {
    display: 'inline',
  },
  tooltip: {
    fontSize: '12px',
  },
  content1: {
    width: '100%',
    maxWidth: 900,
  },
});

const initialOptions = {
  centerContetent: false,
  limitToBounds: false,
  limitToWrapper: true,
};

const Average = () => {
  const classes = useStyles();

  const { level, data } = useContext(DataContex);

  return (
    <div className={classes.mainDiv}>
      <div
        style={{
          border: '5px dashed #1e1c37',
          position: 'relative',
          top: 0,
          left: -60,
          paddingLeft: 70,
          width: '1000px',
        }}
      >
        <TransformWrapper options={initialOptions}>
          {({ zoomIn, zoomOut, resetTransform, ...rest }) => (
            <>
              <div className={classes.tools}>
                <Fab
                  color="primary"
                  aria-label="Powiększ"
                  onClick={() => zoomIn()}
                >
                  <Add />
                </Fab>
                <Fab onClick={() => zoomOut()}>
                  <Remove />
                </Fab>
                <Fab onClick={() => resetTransform()}>
                  <RotateLeftIcon />
                </Fab>
              </div>
              <div className={classes.pnzContainer}>
                <TransformComponent>
                  <RLC />
                </TransformComponent>
              </div>
            </>
          )}
        </TransformWrapper>
      </div>
      <br /> <br />
      <div className={classes.content1}>
        <h3 className={classes.comments}>Uwagi do układu:</h3>
        <ul>
          <li>na rysunku przedstawiono schemat układu szeregowego RLC,</li>
          <li>
            źródło V1 jest źródłem napięcia sinusoidalnego o amplitudzie 10V i
            regulowanej częstotliwości (generator funkcyjny).
          </li>
        </ul>
        <AudioPlayer url={command2} />
      </div>
    </div>
  );
};

export default Average;
