import React, { useContext } from "react";
import { makeStyles } from "@material-ui/core/styles";
import DataContext from "../../../context/DataContext";
import DndContent from "../../DragDropTransfer/subcomponents/VeryHard/content/item";
import AudioPlayer from "../../AudioPlayer";

import command from "../../../data/lector/command4.1.mp3";


const useStyles = makeStyles({
  content: {
    width: "100%",
    margin: "0 auto",
  },
  title: {
    fontWeight: 600,
  },
  command: {
    width: "100%",
    background: "#f5f5f5",
    marginTop: "40px",
    borderRadius: "10px",
    padding: "20px",
  },
});

const VeryHard = () => {
  const classes = useStyles();
  const {} = useContext(DataContext);

  return (
    <div className={classes.content}>
      <div className={classes.command}>
        <p className={classes.title}>Polecenie</p>
        <p>Jeżeli posiadasz w domu miernik uniwersalny, zmierz rezystancję termistora NTC.</p>
        <p>
          W tym celu wykonaj w domu eksperyment: dotknij sondami miernika nóżek termistora NTC i wykonaj pomiar rezystancji. Sprawdź również, co się stanie, gdy podgrzejesz w dłoniach końcówkę tego
          elementu. Następnie wybierz poprawne słowa i wstaw je w odpowiednie miejsce poniższego stwierdzenia.
        </p>
        <div style={{ width: "40%" }}>
          <AudioPlayer url={command} />
        </div>
      </div>
      <div className={classes.command}>
        <p className={classes.title}>Zadanie</p>
        <p>Wniosek:</p>
        <div style={{ display: "flex", alignItems: "center" }}>
          <span>Im wyższa temperatura, tym</span> <DndContent keyData="content1" /> <span>rezystancja. Im niższa temperatura, tym </span> <DndContent keyData="content2" /> <span> rezystancja.</span>
        </div>
      </div>
    </div>
  );
};

export default VeryHard;
