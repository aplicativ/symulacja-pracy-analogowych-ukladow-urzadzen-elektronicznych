import React, { useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import StepLabel from '@mui/material/StepLabel';
import { Button } from '@material-ui/core';
import Stepper from '@mui/material/Stepper';
import Step from '@mui/material/Step';
import DataContext from '@context/DataContext';
import { stepperLabel } from '../../../../data/features';

import { Suspense } from 'react';
import { OrbitControls } from '@react-three/drei/core';
import { Canvas } from '@react-three/fiber';
import Dialog3D from '../../../3DDialog/index';
import Scene from '../../../3dModels/Scene.jsx';

const useStyles = makeStyles({
  header: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    margin: '10px 0',
  },
  buttonContainer: {
    flex: 1,
    width: '100%',
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center',
    gap: 15
  },
  tooltip: {
    fontSize: '12px',
  },
});

const StepperTop = () => {
  const classes = useStyles();
  const { level, data, setState, state, getImagePath } =
    useContext(DataContext);

  const { step } = data;

  const getStepsLevel = () => {
    return step == 0 ? 0 : step - 1;
  };

  const nextStep = () => {
    const newState = {
      ...state,
      [level]: { ...state[level], step: step + 1 },
    };
    setState(newState);
  };

  const prevStep = () => {
    const newState = { ...state, [level]: { ...state[level], step: step - 1 } };

    setState(newState);
  };

  return (
    <div className={classes.header}>
      <div style={{ flex: 0.4 }} />
      <Stepper
        nonLinear
        style={{ flex: 1 }}
        activeStep={getStepsLevel()}
        alternativeLabel
      >
        {stepperLabel.map((label) => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>
      <div className={classes.buttonContainer}>
        <div className={classes.button}>
          <Dialog3D className={classes.button} title="Model 3D">
            <Canvas
              camera={{ position: [0, 10, 0] }}
              style={{ height: '1200px' }}
            >
              <OrbitControls enableZoom={true} />
              <ambientLight intensity={0.5} />
              <directionalLight position={[-2, 5, 2]} />
              <Suspense fallback={null}>
                <Scene getImagePath={getImagePath} />
              </Suspense>
            </Canvas>
          </Dialog3D>
        </div>
        <Button
          startIcon={<ArrowBackIcon />}
          variant="contained"
          color="primary"
          disabled={step == 1}
          onClick={() => prevStep()}
        ></Button>

        <Button
          variant="contained"
          color="primary"
          disabled={step == 2}
          endIcon={<ArrowForwardIcon />}
          onClick={() => nextStep()}
        ></Button>
      </div>
    </div>
  );
};

export default StepperTop;
