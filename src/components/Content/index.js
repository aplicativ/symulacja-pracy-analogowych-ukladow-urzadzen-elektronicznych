import React, { useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@mui/material/Button';

import Validation from '../Validation';
import ModalContext from '@context/ModalContext';
import Tooltip from '@material-ui/core/Tooltip';
import DataContext from '../../context/DataContext';
import Easy from './subcomponents/Easy';
import Average from './subcomponents/Average';
import Hard from './subcomponents/Hard';
import VeryHard from './subcomponents/VeryHard';

const useStyles = makeStyles({
  content: {
    width: '100%',
    margin: '0 auto',
  },
  tooltip: {
    fontSize: '12px',
  },
});

const Content = () => {
  const classes = useStyles();
  const { level, getImagePath } = useContext(DataContext);
  const { setIsInstructionOpen } = useContext(ModalContext);

  return (
    <div
      style={{ maxWidth: level !== 'hard' && 900 }}
      className={classes.content}
    >
      <>
        {level !== 'veryHard' && level !== 'hard' && (
          <Tooltip
            title={
              <span className={classes.tooltip}>
                Kliknij, aby zapoznać się z poleceniem.
              </span>
            }
            arrow={true}
            placement={'top'}
          >
            <Button
              style={{
                width: '100%',
                // maxWidth: "700px",
                margin: '20px auto 20px auto',
              }}
              onClick={() => setIsInstructionOpen(true)}
              variant="outlined"
            >
              Polecenie
            </Button>
          </Tooltip>
        )}
      </>
      {level === 'easy' && <Easy />}
      {level === 'average' && <Average />}
      {level === 'hard' && <Hard />}
      {level === 'veryHard' && <VeryHard />}

      <div style={{ marginTop: '20px', marginBottom: '40px' }}>
        <Validation />
      </div>
    </div>
  );
};

export default Content;
