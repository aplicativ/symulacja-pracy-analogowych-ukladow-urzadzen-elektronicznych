import { makeStyles } from '@material-ui/core/styles'

const drawerWidth = 240

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
  },
  appBar: {
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#396CEA',
    // top: 'inherit',
  },
  // appBarShift: {
  //   width: `calc(100% - ${drawerWidth}px)`,
  //   marginLeft: drawerWidth,
  //   transition: theme.transitions.create(['margin', 'width'], {
  //     easing: theme.transitions.easing.easeOut,
  //     duration: theme.transitions.duration.enteringScreen,
  //   }),
  // },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  hide: {
    display: 'none',
  },

  mainButton: {
    color: 'white',
    fontWeight: '600',
    fontSize: '15px',
  },
  menuButtonMenu: {
    // marginRight: theme.spacing(2),
    position: "absolute",
    left: "20px",
    top: "10px"
  },
  menuButtonFullscreen: {
    position: "absolute",
    right: "20px",
    top: "10px"
  },
  tooltip: {
    fontSize: '12px',
  },
}))

export default useStyles
