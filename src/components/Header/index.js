import React, { useContext } from 'react';
import clsx from 'clsx';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import FullscreenIn from '@material-ui/icons/Fullscreen';
import FullscreenExit from '@material-ui/icons/FullscreenExit';
import Tooltip from '@material-ui/core/Tooltip';
import DataContex from '../../context/DataContext';
import useStyles from './styles';

const tabProps = (index) => {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
};

const Header = ({ isOpenNavigation, setIsOpenNavigation }) => {
  const classes = useStyles();
  const { setState, level, state, isFullscreen, setIsFullscreen } =
    useContext(DataContex);

  const handleLevelChange = (event, newValue) => {
    setState({ ...state, level: newValue });
  };
  const handleChangeFullscreen = () => {
    setIsFullscreen(!isFullscreen);
  };

  return (
    <AppBar
      position="fixed"
      className={clsx(classes.appBar, {
        [classes.appBarShift]: isOpenNavigation,
      })}
    >
      <Toolbar className={classes.toolbar}>
        <Tabs value={level} onChange={handleLevelChange} aria-label="">
          <Tab
            className={classes.mainButton}
            value="easy"
            label="UKŁADY PRACY WZMACNIACZA OPERACYJNEGO"
            {...tabProps(0)}
          />
          <Tab
            className={classes.mainButton}
            value="average"
            label="UKŁAD RLC"
            {...tabProps(1)}
          />
          <Tab
            className={classes.mainButton}
            value="hard"
            label="Budowanie układu"
            {...tabProps(2)}
          />
          <Tab
            className={classes.mainButton}
            value="veryHard"
            label="Eksperyment w domu"
            {...tabProps(3)}
          />
        </Tabs>
      </Toolbar>
      <IconButton
        color="inherit"
        aria-label="open drawer"
        onClick={() => setIsOpenNavigation(!isOpenNavigation)}
        edge="start"
        className={clsx(
          classes.menuButtonMenu,
          isOpenNavigation && classes.hide
        )}
      >
        <MenuIcon />
      </IconButton>
      <Tooltip
        title={
          isFullscreen ? (
            <span className={clsx(classes.tooltip)}>Wyłącz tryb pełnoekranowy</span>
          ) : (
            <span className={clsx(classes.tooltip)}>Włącz tryb pełnoekranowy</span>
          )
        }
        arrow={true}
        placement={'bottom'}
      >
        <IconButton
          color="inherit"
          aria-label="open drawer"
          onClick={handleChangeFullscreen}
          edge="start"
          className={clsx(classes.menuButtonFullscreen)}
        >
          {isFullscreen ? <FullscreenExit /> : <FullscreenIn />}
        </IconButton>
      </Tooltip>
    </AppBar>
  );
};

export default Header;
