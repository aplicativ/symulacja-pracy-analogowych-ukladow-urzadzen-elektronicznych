import React, { useContext } from 'react';

// Libraries
import { DragDropContext } from 'react-beautiful-dnd';

// Data and context
import DataContext from '../../context/DataContext';
import dndSound from '../../data/audio/dnd-sound.mp3'

const DragDropTransfer = ({ children }) => {
  const { columnsInTask, setColumnsInTask, level, getImagePath } = useContext(DataContext);

  const sound = new Audio(getImagePath(dndSound))

  const onDragEnd = ({ source, destination }) => {
    // Make sure we have a valid destination
    if (destination === undefined || destination === null) return null;

    // Make sure we're actually moving the item
    if (
      source.droppableId === destination.droppableId &&
      destination.index === source.index
    )
      return null;

    // Set start and end variables
    const start = columnsInTask[source.droppableId];
    const end = columnsInTask[destination.droppableId];

    // If start is the same as end, we're in the same column
    if (start === end) {
      // Move the item within the list
      // Start by making a new list without the dragged item

      const newList = start.list.filter((_, idx) => idx !== source.index);

      // Then insert the item at the right location
      newList.splice(destination.index, 0, start.list[source.index]);

      // Then create a new copy of the column object
      const newCol = {
        id: start.id,
        list: newList,
      };

      // Update the state
      setColumnsInTask((state) => ({ ...state, [newCol.id]: newCol }));
      return null;
    } else {
      // If start is different from end, we need to update multiple columns
      // Filter the start list like before
      let newStartList = [];
      let newEndList = [];
      if (source.droppableId === 'menu') {
        newStartList = start.list.filter((_, idx) => idx !== source.index);
        const prevNewEndList = end.list;
        if (prevNewEndList.length) {
          newStartList.push(prevNewEndList[0]);
        }
        newEndList.push(start.list[source.index]);

        sound.play()
      } else if (source.droppableId !== 'menu' && end.id !== 'menu') {
        const prevNewStartList = start.list;
        const prevNewEndList = end.list;

        newStartList = start.list.filter((_, idx) => idx !== source.index);
        newEndList = [];

        if (prevNewEndList.length) {
          newStartList.push(prevNewEndList[0]);
        }
        if (start.list.length) {
          newEndList.push(prevNewStartList[0]);
        }
        sound.play()
      } else {
        newStartList = start.list.filter((_, idx) => idx !== source.index);
        newEndList = end.list;
        newEndList.splice(destination.index, 0, start.list[source.index]);
      }

      // Create a new start column
      const newStartCol = {
        id: start.id,
        list: newStartList,
      };

      // Make a new end list array
      // const newEndList = end.list;
      // const prevNewEndList = end.list;
      // console.log(prevNewEndList,prevNewEndList);
      // const newEndList = [];

      // Insert the item into the end list
      // newEndList.splice(destination.index, 0, start.list[source.index]);

      // Create a new end column
      const newEndCol = {
        id: end.id,
        list: newEndList,
      };

      // Update the state
      setColumnsInTask((state) => ({
        ...state,
        [newStartCol.id]: newStartCol,
        [newEndCol.id]: newEndCol,
      }));
      return null;
    }
  };

  return <DragDropContext onDragEnd={onDragEnd}>{children}</DragDropContext>;
};

export default DragDropTransfer;
