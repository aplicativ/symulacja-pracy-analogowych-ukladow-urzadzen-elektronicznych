import React, { useContext } from 'react';

// Libraries
import { Draggable } from 'react-beautiful-dnd';

// Components and mui
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';

// Style
import { makeStyles } from '@material-ui/core/styles';
import DataContext from '../../../../../context/DataContext';

const useStyles = makeStyles((theme) => ({
  listItem: {
    background: '#fff',
    margin: '0',
    padding: '10px',
    width: '200px',
    maxHeight: '250px',
    borderRadius: '10px',
    boxShadow: '0px 4px 12px rgba(0, 0, 0, 0.18)',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },

  imgContainer: {
    width: '70%',

    '& img': {
      width: '100%',
      maxHeight: '200px',
    },
  },

  listItemDragging: {
    background: '#fff',
    margin: '0',
    padding: '10px',
    width: '120px',
    borderRadius: '10px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  imgContainerDragging: {
    width: '70%',

    '& img': {
      width: '100%',
    },
  },
  list: {
    padding: 0,
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  tooltip: {
    fontSize: '12px',
  },
}));

const ListItemCustom = ({ itemObject, index }) => {
  const { getImagePath } = useContext(DataContext);
  const classes = useStyles();

  return (
    <Draggable draggableId={itemObject.key} key={itemObject.key} index={index}>
      {(provided, snapshot) => (
        <ListItem
          className={classes.list}
          key={itemObject.key}
          role={undefined}
          dense
          button
          ContainerComponent="li"
          ContainerProps={{ ref: provided.innerRef }}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
        >
          <ListItemText
            primary={
              !snapshot.isDragging ? (
                <div type="body2" className={classes.listItem}>
                  <div
                    style={{
                      width:
                        (itemObject.text === 'rezystor o wartości 1 kΩ (2)' &&
                          '30px') ||
                        (itemObject.text === 'przewody połączeniowe (4)' &&
                          '20%') ||
                        (itemObject.text === 'przewody połączeniowe (2)' &&
                          '30%'),
                    }}
                    className={classes.imgContainer}
                  >
                    <img crossOrigin='anonymous' src={getImagePath(itemObject.icon)} />
                  </div>
                  <div>{itemObject.text}</div>
                </div>
              ) : (
                <div type="body2" className={classes.listItemDragging}>
                  <div
                    style={{
                      width:
                        itemObject.text === 'rezystor o wartości 1 kΩ (2)' &&
                        '30px',
                    }}
                    className={classes.imgContainer}
                  >
                    <img crossOrigin='anonymous' src={getImagePath(itemObject.icon)} />
                  </div>
                </div>
              )
            }
            style={{ paddingLeft: '0 !import', paddingRight: '0 !import' }}
          />
          <ListItemSecondaryAction
            style={{ width: '0' }}
          ></ListItemSecondaryAction>
        </ListItem>
      )}
    </Draggable>
  );
};

export default ListItemCustom;
