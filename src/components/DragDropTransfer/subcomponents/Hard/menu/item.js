import React, { useContext } from 'react';

// Libraries
import { Droppable } from 'react-beautiful-dnd';

// Components and mui
import List from '@material-ui/core/List';
import ListItem from './list';
import RootRef from '@material-ui/core/RootRef';

// Data and context
import DataContext from '../../../../../context/DataContext';

// Style
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  list: {
    width: '100%',
    padding: '0',
    backgroundColor: '#ffffff',
    minHeight: '100vh',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
}));

const Item = ({ keyData }) => {
  const { columnsInTask, state, level } = useContext(DataContext);
  const { [keyData]: data } = columnsInTask;
  const classes = useStyles();

  const sortAlphabetically = data.list.sort((a, b) =>
    a.text.localeCompare(b.text)
  );

  return (
    <Droppable droppableId={data.id}>
      {(provided) => (
        <RootRef rootRef={provided.innerRef}>
          <List className={classes.list}>
            {sortAlphabetically.map((itemObject, index) => {
              if (itemObject.step === state[level].step) {
                return (
                  <ListItem
                    key={itemObject + '_' + index}
                    index={index}
                    itemObject={itemObject}
                  />
                );
              }
            })}
            {provided.placeholder}
          </List>
        </RootRef>
      )}
    </Droppable>
  );
};

export default Item;
