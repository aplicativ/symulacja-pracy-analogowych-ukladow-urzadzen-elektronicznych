import React, { useContext } from 'react';

// Libraries
import { Droppable } from 'react-beautiful-dnd';

// Components and mui
import List from '@material-ui/core/List';
import ListItem from './list';
import RootRef from '@material-ui/core/RootRef';

// Data and context
import DataContext from '../../../../../context/DataContext';

// Style
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  list: {
    borderRadius: '5px',
    width: '100%',
    height: '100%',
    padding: '0',
    minHeight: '30px',

    '& li': {
      padding: 0,
      width: '100%',
      height: '100%',

      '& .MuiButtonBase-root': {
        padding: '0',
        width: '100%',
        height: '100%',

        '& span': {
          padding: 0,
          width: '100%',
          height: '100%',
        },
      },
    },
  },
}));

const Item = ({ keyData, disabled }) => {
  const { columnsInTask } = useContext(DataContext);
  const { [keyData]: data } = columnsInTask;
  const classes = useStyles();
  return (
    <Droppable isDropDisabled={disabled} droppableId={data.id}>
      {(provided, snapshot) => (
        <RootRef rootRef={provided.innerRef}>
          <List
            style={{ background: snapshot.isDraggingOver && 'rgba(255,255,255, 0.8)' }}
            className={classes.list}
          >
            {data.list.map((itemObject, index) => {
              return (
                <ListItem
                  key={itemObject + '_' + index}
                  index={index}
                  itemObject={itemObject}
                  keyData={keyData}
                  disabled={disabled}
                />
              );
            })}
          </List>
        </RootRef>
      )}
    </Droppable>
  );
};

export default Item;
