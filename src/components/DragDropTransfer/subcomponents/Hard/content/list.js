import React, { useContext } from 'react';

// Libraries
import { Draggable } from 'react-beautiful-dnd';

// Components and mui
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';

// Data and context
import DataContext from '../../../../../context/DataContext';

// Style
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  listItem: {
    position: 'relative',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    width: '100%',
    height: '100%',
  },
  imgContainer: {
    width: '100%',
    height: '100%',

    '& img': {
      width: '100%',
      height: '100%',
      maxHeight: '415px',
    },
  },
}));

const ListItemCustom = ({ itemObject, index, keyData, disabled }) => {
  const classes = useStyles();
  const { columnsInTask, setColumnsInTask, data, getImagePath} = useContext(DataContext);

  return (
    <Draggable
      isDragDisabled={disabled}
      draggableId={itemObject.key}
      key={itemObject.key}
      index={index}
    >
      {(provided, snapshot) => (
        <ListItem
          className={classes.list}
          key={itemObject.key}
          role={undefined}
          dense
          button
          ContainerComponent="li"
          ContainerProps={{ ref: provided.innerRef }}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
        >
          <ListItemText
            primary={
              <div className={classes.imgContainer}>
                <img crossOrigin='anonymous' src={getImagePath(itemObject.img)} />
              </div>
            }
            style={{ paddingLeft: '0 !import', paddingRight: '0 !import' }}
          />
          <ListItemSecondaryAction
            style={{ width: '0' }}
          ></ListItemSecondaryAction>
        </ListItem>
      )}
    </Draggable>
  );
};

export default ListItemCustom;
