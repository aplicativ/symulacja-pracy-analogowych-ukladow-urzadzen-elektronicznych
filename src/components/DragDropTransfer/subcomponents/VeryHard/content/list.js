import React, { useContext } from 'react';

// Libraries
import { Draggable } from 'react-beautiful-dnd';

// Components and mui
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';

// Data and context
import DataContext from '../../../../../context/DataContext';

// Style

// Style
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  listItem: {
    position: 'relative',
    fontSize: '14px',
  },
}));

const ListItemCustom = ({ itemObject, index, keyData }) => {
  const classes = useStyles();
  const { columnsInTask, setColumnsInTask } = useContext(DataContext);

  return (
    <Draggable draggableId={itemObject.key} key={itemObject.key} index={index}>
      {(provided, snapshot) => (
        <ListItem
          className={classes.list}
          key={itemObject.key}
          role={undefined}
          dense
          button
          ContainerComponent="li"
          ContainerProps={{ ref: provided.innerRef }}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
        >
          <ListItemText
            primary={
              <div type="body2" className={classes.listItem}>
                <p>{itemObject.text}</p>
              </div>
            }
            style={{ paddingLeft: '0 !import', paddingRight: '0 !import' }}
          />
          <ListItemSecondaryAction
            style={{ width: '0' }}
          ></ListItemSecondaryAction>
        </ListItem>
      )}
    </Draggable>
  );
};

export default ListItemCustom;
