import React, { useContext } from 'react';

// Libraries
import { Droppable } from 'react-beautiful-dnd';

// Components and mui
import List from '@material-ui/core/List';
import ListItem from './list';
import RootRef from '@material-ui/core/RootRef';

// Data and context
import DataContext from '../../../../../context/DataContext';

// Style
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  list: {
    backgroundColor: '#ffffff',
    borderRadius: '5px',
    width: '15%',
    minHeight: '50px',
    margin: '0 20px',
    padding: '0',
    border: '3px dashed #D9D9D9',

    '& li': {
      textAlign: 'center',

      '& .MuiButtonBase-root': {
        padding: '0',
        textAlign: 'center',

        '& span': {
          display: 'flex',
          justifyContent: 'center',
        },
      },
    },
  },
}));

const Item = ({ keyData }) => {
  const { columnsInTask } = useContext(DataContext);
  const { [keyData]: data } = columnsInTask;
  const classes = useStyles();
  return (
    <Droppable droppableId={data.id}>
      {(provided) => (
        <RootRef rootRef={provided.innerRef}>
          <List className={classes.list}>
            {data.list.map((itemObject, index) => {
              return (
                <ListItem
                  key={itemObject + '_' + index}
                  index={index}
                  itemObject={itemObject}
                  keyData={keyData}
                />
              );
            })}
            {provided.placeholder}
          </List>
        </RootRef>
      )}
    </Droppable>
  );
};

export default Item;
