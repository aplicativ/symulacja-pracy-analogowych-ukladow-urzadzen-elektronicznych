import React from 'react';

// Libraries
import { Draggable } from 'react-beautiful-dnd';

// Components and mui
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';

// Style
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  listItem: {
    background: '#fff',
    margin: '0',
    padding: 0,
    width: '150px',
    height: '50px',
    borderRadius: '10px',
    boxShadow: '0px 4px 12px rgba(0, 0, 0, 0.18)',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',

    '& p': {
      fontSize: '12px',
      fontWeight: 400,
    },
  },
  list: {
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    padding: 0,
    marginTop: '20px'
  },
}));

const ListItemCustom = ({ itemObject, index }) => {
  const classes = useStyles();

  

  return (
    <Draggable draggableId={itemObject.key} key={itemObject.key} index={index}>
      {(provided) => (
        <ListItem
          className={classes.list}
          key={itemObject.key}
          role={undefined}
          dense
          button
          ContainerComponent="li"
          ContainerProps={{ ref: provided.innerRef }}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
        >
          <ListItemText
            primary={
              <div type="body2" className={classes.listItem}>
                <p>{itemObject.text}</p>
              </div>
            }
            style={{ paddingLeft: '0 !import', paddingRight: '0 !import' }}
          />
          <ListItemSecondaryAction
            style={{ width: '0' }}
          ></ListItemSecondaryAction>
        </ListItem>
      )}
    </Draggable>
  );
};

export default ListItemCustom;
