import React, { useState } from 'react';
import Dialog from '@material-ui/core/Dialog';

import { makeStyles } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';

import CloseIcon from '@mui/icons-material/Close';
import IconButton from '@mui/material/IconButton';
import AudioPlayer from '../AudioPlayer';

import instruction from '../../data/lector/instruction.mp3';

const Modal = ({ isOpen, setIsOpen }) => {
  const useStyles = makeStyles((theme) => ({
    inner: {
      padding: '30px 30px',
    },
    heading: {
      fontSize: theme.typography.pxToRem(15),
      flexBasis: '33.33%',
      flexShrink: 0,
    },
    secondaryHeading: {
      fontSize: theme.typography.pxToRem(15),
      color: theme.palette.text.secondary,
    },
    description: {
      padding: '15px',
    },
    close: {
      position: 'absolute',
      top: '10px',
      right: '10px',
    },
  }));
  const classes = useStyles();

  const handleClose = () => {
    setIsOpen(false);
  };

  return (
    <Dialog
      style={{ zIndex: '999999' }}
      maxWidth="lg"
      fullWidth
      onClose={handleClose}
      aria-labelledby="simple-dialog-title"
      open={isOpen}
    >
      <div className={classes.inner}>
        <div className={classes.close}>
          <IconButton
            onClick={handleClose}
            aria-label="close"
            component="span"
            style={{ padding: '2px' }}
          >
            <CloseIcon
              style={{
                stroke: '#757575',
                strokeWidth: 2,
              }}
            />
          </IconButton>
        </div>
        <Typography variant="h2" component="h2">
          Instrukcja obsługi do programu ćwiczeniowego „Dobieranie analogowych
          układów urządzeń elektronicznych”
        </Typography>
        <br />
        <p className={classes.paragraph}>
          Przed przystąpieniem do wykonania ćwiczenia zapoznaj się z poleceniem.
        </p>
        <p className={classes.paragraph}>
          Dla wygody korzystania z programu ćwiczeniowego kliknij ikonę trybu
          pełnoekranowego. Umożliwia ona przeglądarce zajęcie całego ekranu.
        </p>
        {/* <p className={classes.paragraph}>
          W celu odsłuchania treści zawartych w programie ćwiczeniowym wybierz
          ikonę „Odsłuchaj”. W dolnym menu znajdziesz również ikony, po
          kliknięciu na które będziesz mógł zapoznać się z innymi materiałami
          multimedialnymi z e-zasobu.
        </p> */}
        <p className={classes.paragraph}>
          Jeśli chcesz zapisać postępy swojej pracy użyj ikony „Pobierz listę
          kroków”, a plik zapisz na dysku komputera.
        </p>
        <p className={classes.paragraph}>
          Program ćwiczeniowy umożliwia również zapisanie całej wykonanej pracy,
          zrzut ekranu oraz ponowne wykonanie ćwiczenia. Po
          wykonaniu zadania, niezależnie od poprawności wyniku, otrzymasz informację
          zwrotną.
        </p>
        <p>
          W przypadku korzystania wyłącznie z klawiatury należy użyć poniższych
          klawiszy:
        </p>
        <ul>
          <li>Tab - poruszanie się do przodu po elementach</li>
          <li>Shift + Tab - poruszanie się do tyłu po elementach</li>
          <li>Spacja - podnoszenie i upuszczanie elementów</li>
          <li>Escape - anulowanie przeciągania</li>
          <li>
            Strzałki - przenoszenie elementów do sąsiadujących stref upuszczania
          </li>
        </ul>
        <AudioPlayer
          url={instruction}
          label="Instrukcja obsługi do programu ćwiczeniowego"
        />
      </div>
    </Dialog>
  );
};

export default Modal;
