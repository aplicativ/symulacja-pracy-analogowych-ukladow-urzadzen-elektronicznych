import React, { useContext, useState } from 'react';
import ReactAudioPlayer from 'react-audio-player';
import DataContext from '../../context/DataContext';
import audio from '../../data/audio/theme.mp3';

const ThemeSound = ({ label, isPlaying }) => {
  const { getImagePath } = useContext(DataContext);
  return (
    <>
      {isPlaying && (
        <ReactAudioPlayer
          src={getImagePath(audio)}
          controls={false}
          autoPlay
          loop={true}
          volume={0.5}
        />
      )}
    </>
  );
};

export default ThemeSound;
