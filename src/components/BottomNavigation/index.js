import React, { useContext, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import GetAppIcon from '@material-ui/icons/GetApp';
import SaveIcon from '@material-ui/icons/Save';
import PatternIcon from '@mui/icons-material/Pattern';
import CameraAltIcon from '@material-ui/icons/CameraAlt';
import { BrowserRouter as Router } from 'react-router-dom';
import Patterns from './Patterns';
import saveScreen from '@lib/screen';

import MusicNoteIcon from '@mui/icons-material/MusicNote';
import MusicOffIcon from '@mui/icons-material/MusicOff';
import initialState from './../../data/initialState';
import { setLocalStorage } from '@lib/localStorage';
import SyncIcon from '@mui/icons-material/Sync';
import InfoIcon from '@mui/icons-material/Info';
import DataContext from '../../context/DataContext';
import Modal from '@components/ModalInstruction';
import Modal2 from '@components/KnowledgeBase';
import ModalContext from '../../context/ModalContext';
import Tooltip from '@material-ui/core/Tooltip';

import { taskHardMenu, taskVeryHardMenu } from '../../data/features';

import ThemeSound from '../ThemeSound';
import printSteps from './../../lib/pdf';

const useStyles = makeStyles({
  root: {
    width: '100%',
    height: '65px',
    '& button': {
      paddingTop: '30px 0',
    },
  },
  tooltip: {
    fontSize: '12px',
  },
});

export default function SimpleBottomNavigation() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const [isOpenModalInst, setIsOpenModalInst] = useState(0);
  const { isKnowledgeBaseOpen, setIsKnowledgeBaseOpen } =
    useContext(ModalContext);
  const [isPlaying, setIsPlaying] = useState(false);
  const {
    appID,
    state,
    setState,
    level,
    setColumnsInTask,
    hardTaskDnd,
    veryHardTaskDnd,
  } = useContext(DataContext);

  const resetState = () => {
    const resetState = { ...state, [level]: initialState[level] };

    if (level === 'easy') {
      setState(resetState);
    }

    if (level === 'average') {
      setState(resetState);
    }
    if (level === 'hard') {
      setColumnsInTask(taskHardMenu);
      setState(resetState);
    }
    if (level === 'veryHard') {
      setColumnsInTask(taskVeryHardMenu);
      setState(resetState);
    }
  };

  const updateLocalStorage = () => {
    setLocalStorage(appID, state);
    setLocalStorage(appID + 'hard', hardTaskDnd);
    setLocalStorage(appID + 'veryHard', veryHardTaskDnd);
  };

  return (
    <>
      <Modal isOpen={isOpenModalInst} setIsOpen={setIsOpenModalInst} />
      <Modal2 isOpen={isKnowledgeBaseOpen} setIsOpen={setIsKnowledgeBaseOpen} />
      <Router>
        <BottomNavigation
          value={value}
          onChange={(event, newValue) => {
            setValue(newValue);
          }}
          showLabels
          className={classes.root}
        >
          <Tooltip
            title={
              <span className={classes.tooltip}>
                Kliknij, aby zobaczyć instrukcję obsługi programu
              </span>
            }
            arrow={true}
            placement={'top'}
          >
            <BottomNavigationAction
              label="Instrukcja"
              icon={<InfoIcon />}
              onClick={() => setIsOpenModalInst(true)}
            />
          </Tooltip>

          <Patterns />

          <Tooltip
            title={
              <span className={classes.tooltip}>
                Kliknij, aby zapoznać się z dodatkowymi materiałami oraz
                multimediami
              </span>
            }
            arrow={true}
            placement={'top'}
          >
            <BottomNavigationAction
              label="Baza wiedzy"
              icon={<PatternIcon />}
              onClick={() => setIsKnowledgeBaseOpen(true)}
            />
          </Tooltip>
          <Tooltip
            title={
              <span className={classes.tooltip}>
                Kliknij, aby pobrać plik PDF z Twoimi wyborami
              </span>
            }
            arrow={true}
            placement={'top'}
          >
            <BottomNavigationAction
              onClick={() => printSteps()}
              label="Pobierz listę kroków"
              icon={<GetAppIcon />}
            />
          </Tooltip>
          <Tooltip
            title={
              <span className={classes.tooltip}>
                Kliknij, aby zapisać postęp pracy. Po zamknięciu karty
                przeglądarki i ponownym jej otwarciu, pozwoli to na powrót do
                tego samego miejsca (powrót do ostatnio dokonanych wyborów)
              </span>
            }
            arrow={true}
            placement={'top'}
          >
            <BottomNavigationAction
              onClick={() => updateLocalStorage()}
              label="Zapisz efekty pracy"
              icon={<SaveIcon />}
            />
          </Tooltip>
          <Tooltip
            title={
              <span className={classes.tooltip}>
                Kliknij, aby zrobić zrzut ekranu
              </span>
            }
            arrow={true}
            placement={'top'}
          >
            <BottomNavigationAction
              onClick={() => saveScreen('app')}
              label="Zrzut ekranu"
              icon={<CameraAltIcon />}
            />
          </Tooltip>
          <Tooltip
            title={
              <span className={classes.tooltip}>
                Kliknij, aby usunąć wszystkie wybory i ponownie rozwiązać
                ćwiczenie
              </span>
            }
            arrow={true}
            placement={'top'}
          >
            <BottomNavigationAction
              onClick={() => resetState()}
              label="Spróbuj ponownie"
              icon={<SyncIcon />}
            />
          </Tooltip>
          <Tooltip
            title={
              <span className={classes.tooltip}>
                Kliknij, aby włączyć/wyłączyć dźwięk w tle
              </span>
            }
            arrow={true}
            placement={'top'}
          >
            <BottomNavigationAction
              label={isPlaying ? 'Wyłącz' : 'Włącz'}
              onClick={() => setIsPlaying(!isPlaying)}
              icon={isPlaying ? <MusicOffIcon /> : <MusicNoteIcon />}
            ></BottomNavigationAction>
          </Tooltip>

          <ThemeSound isPlaying={isPlaying} />
        </BottomNavigation>
      </Router>
    </>
  );
}
