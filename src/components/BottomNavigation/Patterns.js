import React, { useContext } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import { makeStyles } from '@material-ui/core/styles';
import { BrowserRouter as Router } from 'react-router-dom';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import CreateIcon from '@material-ui/icons/Create';
import DataContext from './../../context/DataContext';

import CloseIcon from '@mui/icons-material/Close';
import IconButton from '@mui/material/IconButton';
import wzorku1 from '../../components/Content/subcomponents/img/wzórku1.svg';
import wzorku2 from '../../components/Content/subcomponents/img/wzórku2.svg';
import modulImpedancji from '../../components/Content/subcomponents/img/modulImpedancji.svg';
import prawoOhma from '../../components/Content/subcomponents/img/prawoOhma.svg';
import czestotliwoscRezonansowa from '../../components/Content/subcomponents/img/czestotliwoscRezonansowa.svg';
import reakrancjaPojemnosciowa from '../../components/Content/subcomponents/img/reakrancjaPojemnosciowa.svg';
import reaktancjaIndukcyjnaCewki from '../../components/Content/subcomponents/img/reaktancjaIndukcyjnaCewki.svg';
import Tooltip from '@material-ui/core/Tooltip';

const useStyles = makeStyles((theme) => ({
  dialog: {
    zIndex: '999999',
    '& div': {
      '& div': {
        maxWidth: '100%',
      },
    },
  },
  submitcancel: {
    textAlign: 'right',
    '& button': {
      color: '#b54d2d',
      border: '1px solid #b54d2d',
      margin: '10px',
      padding: '5px 20px',
      '& a': {
        color: '#b54d2d',
        textDecoration: 'none',
      },
      '&:hover': {
        border: '1px solid #6d2f1b',
        backgroundColor: 'rgba(109, 47, 27, 0.1);',
      },
    },
  },
  addResourceMaterial: {
    padding: '10px',
    borderRadius: '5px',
    border: 'none',
    margin: '15px 0',
    backgroundColor: 'rgba(109, 47, 27, 0.3)',
    '& a': {
      textDecoration: 'none',
      color: '#000',
    },
  },

  task: {
    padding: '10px',
  },
  close: {
    position: 'absolute',
    top: '15px',
    right: '15px',
  },
  img: {
    width: '100px',
  },
  img2: {
    width: '190px',
  },
  img3: {
    width: '70px',
  },
  img4: {
    width: '170px',
  },
  tooltip: {
    fontSize: '12px',
  },
}));

export default function FormDialog(props) {
  let { form } = props;
  const classes = useStyles();
  const { getImagePath } = useContext(DataContext);

  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <Router>
      <Tooltip
        title={
          <span className={classes.tooltip}>Kliknij, aby zobaczyć wzory</span>
        }
        arrow={true}
        placement={'top'}
      >
        <BottomNavigationAction
          label="Wzory"
          icon={<CreateIcon />}
          onClick={handleClickOpen}
          className={classes.patterns}
          showLabel
        />
      </Tooltip>
      <Dialog
        className={classes.dialog}
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
        style={{ zIndex: '999999' }}
      >
        <DialogContent>
          <div className={classes.close}>
            <IconButton
              onClick={handleClose}
              aria-label="close"
              component="span"
            >
              <CloseIcon
                style={{
                  stroke: '#757575',
                  strokeWidth: 2,
                }}
              />
            </IconButton>
          </div>
          <ul>
            <h3 style={{ marginBottom: '30px' }}>Skorzystaj ze wzorów na:</h3>
            <li>wzmocnienie napięciowe pierwszego stopnia:</li>
            <img crossOrigin='anonymous'
              className={classes.img}
              src={getImagePath(wzorku1)}
              alt="wzmocnienie napięciowe pierwszego stopnia"
            />
            <li>wzmocnienie napięciowe drugiego stopnia:</li>
            <img crossOrigin='anonymous'
              className={classes.img}
              src={getImagePath(wzorku2)}
              alt="wzmocnienie napięciowe drugiego stopnia"
            />
            <li>moduł impedancji:</li>
            <img crossOrigin='anonymous'
              className={classes.img2}
              src={getImagePath(modulImpedancji)}
              alt="moduł impedancji"
            />
            <li>prawo Ohma:</li>
            <img crossOrigin='anonymous'
              className={classes.img3}
              src={getImagePath(prawoOhma)}
              alt="prawo Ohma"
            />
            <li>częstotliwość rezonansowa:</li>
            <img crossOrigin='anonymous'
              className={classes.img4}
              src={getImagePath(czestotliwoscRezonansowa)}
              alt="częstotliwość rezonansowa"
            />
            <li>reaktancja indukcyjna cewki:</li>
            <img crossOrigin='anonymous'
              className={classes.img2}
              src={getImagePath(reaktancjaIndukcyjnaCewki)}
              alt="reaktancja indukcyjna cewki"
            />
            <li>reaktancja pojemnościowa:</li>
            <img crossOrigin='anonymous'
              className={classes.img4}
              src={getImagePath(reakrancjaPojemnosciowa)}
              alt="reaktancja pojemnościowa"
            />
          </ul>
        </DialogContent>
        <DialogActions className={`${classes.task} ${classes.submitcancel}`}>
          <Button onClick={handleClose} color="primary">
            Ok
          </Button>
        </DialogActions>
      </Dialog>
    </Router>
  );
}
