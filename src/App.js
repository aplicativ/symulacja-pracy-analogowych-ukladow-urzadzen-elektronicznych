import React, { useState, useEffect } from 'react';
import clsx from 'clsx';
import CssBaseline from '@material-ui/core/CssBaseline';
import BottomNav from './components/BottomNavigation';

import Modal from '@components/Modal';

import DataContex from './context/DataContext';
import ModalContext from './context/ModalContext';

import Header from '@components/Header';
import MainNavigation from '@components/MainNavigation';
import Content from '@components/Content';
import DragDropTransfer from '@components/DragDropTransfer';
import Instruction from '@components/Instruction';
import initialState from './data/initialState';

import StepsPdf from '@components/StepsPdf';

import useStyles from './styles';
import { SnackbarProvider } from 'notistack';
import { MuiThemeProvider, createTheme } from '@material-ui/core/styles';
import {
  setLocalStorage,
  getLocalStorage,
  isValidLocalStorage,
} from '@lib/localStorage';
import { taskHardMenu, taskVeryHardMenu } from './data/features';

const theme = createTheme({
  palette: {
    primary: {
      main: '#1e1c37',
    },
    secondary: {
      main: '#1e1c37',
    },
  },
  typography: {
    fontFamily: ['OpenSans', 'sans-serif'].join(','),
    h2: {
      fontSize: '24px',
      fontWeight: 'bold',
      marginBottom: '10px',
      textAlign: 'center',
      fontFamily: ['Oswald', 'sans-serif'].join(','),
    },
  },
  overrides: {
    MuiTooltip: {
      popper: {
        zIndex: '999999 !important',
      },
    },
  },
});

const App = ({ getImagePath }) => {
  const classes = useStyles();
  const [state, setState] = useState(initialState);
  const { level } = state;
  const { [level]: data } = state;
  const [isOpenNavigation, setIsOpenNavigation] = useState(true);
  const [isFullscreen, setIsFullscreen] = useState(false);
  const [isInstructionOpen, setIsInstructionOpen] = useState(false);
  const [isKnowledgeBaseOpen, setIsKnowledgeBaseOpen] = useState(false);
  const [columns, setColumns] = useState('');
  const [hardTaskDnd, setHardTaskDnd] = useState(taskHardMenu);
  const [veryHardTaskDnd, setVeryHardTaskDnd] = useState(taskVeryHardMenu);

  const appID = 'symulacjaanalogowa-v1.2';

  const modalState = {
    isOpen: false,
    type: null,
    title: null,
    text: null,
  };

  const [modalParams, setModalParams] = useState(modalState);

  useEffect(() => {
    if (isValidLocalStorage(appID)) {
      setState(JSON.parse(getLocalStorage(appID)));
    }
    if (isValidLocalStorage(appID + 'veryHard')) {
      setVeryHardTaskDnd(JSON.parse(getLocalStorage(appID + 'veryHard')));
    }
    if (isValidLocalStorage(appID + 'hard')) {
      setHardTaskDnd(JSON.parse(getLocalStorage(appID + 'hard')));
    }
  }, []);

  let columnsInTask;
  let setColumnsInTask;

  if (level === 'hard') {
    columnsInTask = hardTaskDnd;
    setColumnsInTask = setHardTaskDnd;
  } else if (level === 'veryHard') {
    columnsInTask = veryHardTaskDnd;
    setColumnsInTask = setVeryHardTaskDnd;
  } else {
    columnsInTask = columns;
    setColumnsInTask = setColumns;
  }

  return (
    <MuiThemeProvider theme={theme}>
      <SnackbarProvider
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
      >
        <ModalContext.Provider
          value={{
            modalParams,
            setModalParams,
            isInstructionOpen,
            setIsInstructionOpen,
            isKnowledgeBaseOpen,
            setIsKnowledgeBaseOpen,
          }}
        >
          <DataContex.Provider
            value={{
              state,
              data,
              setState,
              level,
              isFullscreen,
              setIsFullscreen,
              getImagePath,
              appID,
              columnsInTask,
              setColumnsInTask,
              hardTaskDnd,
              veryHardTaskDnd
            }}
          >
            <div
              className={clsx(`${classes.root}`, {
                [classes.fullScreen]: isFullscreen,
              })}
            >
              <CssBaseline />
              <DragDropTransfer>
                <Header
                  isOpenNavigation={isOpenNavigation}
                  setIsOpenNavigation={setIsOpenNavigation}
                />
                <MainNavigation
                  isOpenNavigation={isOpenNavigation}
                  setIsOpenNavigation={setIsOpenNavigation}
                />

                <main
                  className={clsx(classes.content, {
                    [classes.contentShift]: isOpenNavigation,
                  })}
                >
                  <div className={classes.drawerHeader} />
                  <div id="app" className={classes.task}>
                    <Content />
                  </div>
                  <footer
                    className={clsx(classes.footer, {
                      [classes.contentShift]: isOpenNavigation,
                    })}
                  >
                    <BottomNav />
                  </footer>
                </main>
              </DragDropTransfer>
            </div>
            <Modal />
            <StepsPdf />
            <Instruction />
          </DataContex.Provider>
        </ModalContext.Provider>
      </SnackbarProvider>
    </MuiThemeProvider>
  );
};

export default App;
