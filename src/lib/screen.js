import html2canvas from 'html2canvas';

const saveSreen = (id) => {

    const downloadBase64File = (contentType, base64Data, fileName) => {
        const linkSource = `data:${contentType};base64,${base64Data}`;
        const downloadLink = document.createElement("a");
        downloadLink.href = linkSource;
        downloadLink.download = fileName;
        downloadLink.click();
    }

    const container = document.getElementById(id);
    html2canvas(container, { logging: true, letterRendering: 1, allowTaint: false, useCORS: true }).then((canvas) => {
       
        var t = canvas.toDataURL().replace("data:image/png;base64,", "");
        downloadBase64File('image/png', t, 'image');
    })
}


export default saveSreen;
