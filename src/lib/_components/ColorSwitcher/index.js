import React, { useState, useEffect } from "react";

import { makeStyles } from "@material-ui/core/styles";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import { Button } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  colorSwitcher: {},
  colorSwitcherButton: {
    padding: "8px !important",
    minWidth: "46px",
  },
  colorSwitcherButtonInner: {
    width: "30px",
    height: "30px",
    padding: 0,
    display: "inline-block",
    lineHeight: 1,
    backgroundColor: "transparent",
    appearance: "none",
    boxShadow: "none",
    borderRadius: "0",
    border: "none",
    cursor: "pointer",
    textDecoration: "none",
  },
  colorSwitcherTitle: {
    fontSize: "13px",
    textAlign: "center",
  },
  colorSwitcherItem: {
    minWidth: "160px",
    height: "30px",
    marginBottom: "10px",
    cursor: "pointer",
    opacity: 0.8,
    transition: theme.transitions.create(["opacity"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    "&:hover": {
      opacity: 1,
    },
  },
}));

const ColorPicker = ({ productColors, setColor, color }) => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const filtredColors = [];
  const classes = useStyles();

  useEffect(() => {
    if (productColors.length > 0) {
      setColor(productColors[0]);
      filterColors(productColors[0]);
    }
  }, []);

  useEffect(() => {
    if (productColors.length > 0) {
      setColor(productColors[0]);
      filterColors(productColors[0]);
    }
  }, [productColors]);

  const filterColors = (color) => {
    const filtered = productColors.filter((item) => item !== color);
    filtredColors(filtered);
  };

  const handleChangeColor = (newColor) => {
    setColor(newColor);
    setIsModalOpen(false);
    filterColors(newColor);
  };

  const handleClose = () => {
    setIsModalOpen(false);
  };

  return (
    <>
      {productColors.length > 1 && (
        <div className={classes.colorSwitcher}>
          <Dialog open={isModalOpen} onClose={handleClose}>
            <h3 className={classes.colorSwitcherTitle}>Wybierz kolor</h3>
            <DialogContent>
              {filtredColors.map((color) => (
                <div
                  onClick={(e) => handleChangeColor(e.target.dataset.color)}
                  key={color}
                  className={classes.colorSwitcherItem}
                  data-color={color}
                  style={{ backgroundColor: color }}
                ></div>
              ))}
            </DialogContent>
          </Dialog>
          <Button
            onClick={() => setIsModalOpen(!isModalOpen)}
            variant="contained"
            className={classes.colorSwitcherButton}
          >
            <div
              className={classes.colorSwitcherButtonInner}
              style={{ backgroundColor: color }}
              type="button"
            ></div>
          </Button>
        </div>
      )}
    </>
  );
};

export default ColorPicker;
