import Swal from "sweetalert2";
import "./message.css";
import plytka from "./plytka.png";

const MessageRLC = {
  success: () => {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: "btn btn-success",
      },
      buttonsStyling: true,
    });

    swalWithBootstrapButtons
      .fire({
        title: "Gratulacje!",
        text:
          "Ćwiczenie jestf poprawnie rozwiązane. Teraz możesz wykonać kolejne zadanie.",
        icon: "success",
        showCancelButton: false,
        confirmButtonText: "Dodatkowe zadanie",
        cancelButtonText: "Ok!",
        reverseButtons: true,
      })
      .then((result) => {
        if (result.isConfirmed) {
          swalWithBootstrapButtons.fire({
            html:
              "W  ramach  pracy własnej lub pracowni wykonaj podane ćwiczenia.  W  tym  celu możesz wykorzystać elementy elektroniczne, przyrządy pomiarowe i płytkę stykową." +
              "<ul><b>Propozycje ćwiczeń: </b><li>korzystając  z  przyrządów  pomiarowych  (multimetr,  mostek  RLC)  wykonać  pomiar  kilku wartości rezystancji, pojemności i indukcyjności wykorzystywanych w ćwiczeniu,</li><li>dokonać pomiaru  napięć   na   poszczególnych   elementach   dla   amplitudy  napięcia źródła   10V   i częstotliwości 50Hz, 500Hz, 2000Hz,</li><li>wyznaczyć wartość prądu w obwodzie dla powyższych pomiarów,</li><li>wyznaczyć  częstotliwości   rezonansowe   na   podstawie   pomiaru   spadku  napięcia   na rezystorze oraz wyznaczyć charakterystyki częstotliwościowe,</li><li>wyznaczyć częstotliwości rezonansowe dla innych wartości parametrów RLC,</li><li>zmodyfikować układ i powtórzyć pomiary dla układów równoległego RLC oraz połączenia mieszanego,</li><li>dobrać parametry RLC w celu uzyskania częstotliwości rezonansowych, np. 5kHz.</li></ul>",
            imageUrl: plytka,
            imageWidth: 600,
            imageHeight: 700,
          });
        }
      });
  },
  error: (title, text) => {
    Swal.fire({
      title,
      text,
      icon: "error",
      confirmButtonText: "Ok",
    });
  },
};

export default MessageRLC;
