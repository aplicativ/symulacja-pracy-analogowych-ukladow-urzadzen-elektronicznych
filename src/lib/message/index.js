import Swal from "sweetalert2";
import "./message.css";
import plytka from "./plytka.png";

const Message = {
  success: () => {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: "btn btn-success",
      },
      buttonsStyling: true,
    });

    swalWithBootstrapButtons
      .fire({
        title: "Gratulacje!",
        text:
          "Ćwiczenie jest poprawnie rozwiązane. Teraz możesz wykonać kolejne zadanie.",
        icon: "success",
        showCancelButton: false,
        confirmButtonText: "Dodatkowe zadanie",
        cancelButtonText: "Ok!",
        reverseButtons: true,
        backdrop: "rgba(65,172,38,0.3)",
      })
      .then((result) => {
        if (result.isConfirmed) {
          swalWithBootstrapButtons.fire({
            html:
              "W ramach pracy własnej lub pracowni wykonaj podane ćwiczenia. W tym celu możesz wykorzystać elementy elektroniczne, przyrządy pomiarowe i płytkę stykową." +
              "<ul><b>Propozycje ćwiczeń: </b><li>wyznaczyć wzmocnienie napięcia na poszczególnych stopniach wzmacniacza (uwaga: wartości rezystancji stanowią jedynie propozycję i mogą być zmienione dla potrzeb ćwiczenia, np. dobrane, aby uzyskać określone wzmocnienie),</li><li>dokonać pomiaru napięć na wyjściach poszczególnych wzmacniaczy operacyjnych i porównać przebieg z przebiegiem napięcia wejściowego,</li><li>zmodyfikować układ przez zastosowanie na poszczególnych stopniach innych układów pracy (np. nieodwracającego, całkującego czy różniczkującego) oraz zbadać wpływ parametrów rezystancji i pojemności na przebiegi,</li><li>wykonać pomiar przebiegów napięć dla układów różniczkujących przy wymuszeniu przebiegiem prostokątnym.</li></ul>",
            imageUrl: plytka,
            imageWidth: 600,
            imageHeight: 700,
          });
        }
      });
  },
  error: (title, text) => {
    Swal.fire({
      title,
      text,
      icon: "error",
      confirmButtonText: "Spróbuj ponownie",
      backdrop: `rgba(255,0,0,0.6)`,
    });
  },

  warning: (title, text) => {
    Swal.fire({
      title,
      text,
      icon: "warning",
      confirmButtonText: "Spróbuj ponownie",
      confirmButtonColor: "red",
      backdrop: `rgba(255,132,19,0.3)`,
    });
  },
};

export default Message;
