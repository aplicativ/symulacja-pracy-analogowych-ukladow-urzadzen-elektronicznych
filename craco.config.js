const path = require(`path`);
const alias = require(`./src/config/aliases`);
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const SRC = `./src`;
const aliases = alias(SRC);

const resolvedAliases = Object.fromEntries(
  Object.entries(aliases).map(([key, value]) => [key, path.resolve(__dirname, value)])
);

module.exports = {
  webpack: {
    alias: resolvedAliases,
    plugins: {
      remove: ['MiniCssExtractPlugin'],
      add: [
        new MiniCssExtractPlugin({
          filename: 'static/css/main.css',
        }),
      ],
    },
    configure: (webpackConfig) => {
      if (!process.env.DEPLOY) {
        return webpackConfig;
      }
      return {
        ...webpackConfig,
        output: {
          ...webpackConfig.output,
          filename: 'static/js/entry.js',
          libraryTarget: 'amd',
        },
        entry: './src/entry.js',
        optimization: {
          runtimeChunk: false,
          splitChunks: {
            chunks() {
              return false;
            },
          },
        },
      };
    },
  },
};